package com.billennium.CarReservationSystem

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class LoadContextTest extends Specification {

    @Autowired
    private CarReservationSystemApplication system

    def "when context is loaded then all expected beans are created"() {
        expect: "the system is created"
        system
    }
}