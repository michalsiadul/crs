package com.billennium.CarReservationSystem

import com.billennium.CarReservationSystem.entity.ApplicationUser
import com.billennium.CarReservationSystem.entity.ApplicationUserRole
import com.billennium.CarReservationSystem.entity.Location
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchLocationException
import com.billennium.CarReservationSystem.repository.ApplicationUserRepository
import com.billennium.CarReservationSystem.service.ApplicationUserService
import com.billennium.CarReservationSystem.service.LocationService
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.io.Resource
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import spock.lang.Specification

import javax.validation.ValidationException


import static com.billennium.CarReservationSystem.setUp.applicationUser
import static com.billennium.CarReservationSystem.setUp.convertResourceToString
import static com.billennium.CarReservationSystem.setUp.setUpAppUser
import static com.billennium.CarReservationSystem.setUp.setUpLocation
import static com.billennium.CarReservationSystem.setUp.setUpUserRole
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
class AdministratorControllerTestSpock extends Specification {

    @Autowired
    private MockMvc mvc
    @SpringBean
    private LocationService locationService = Mock()
    @SpringBean
    private ApplicationUserRepository applicationUserRepository = Mock()
    @SpringBean
    private ApplicationUserService applicationUserService = Mock()
    @Autowired
    ObjectMapper mapper;
    @Value("classpath:testdata/location/locationRequest.json")
    private Resource locationRequest
    @Value("classpath:testdata/location/locationResponse.json")
    private Resource locationResponse
    @Value("classpath:testdata/location/locationsResponse.json")
    private Resource locationsResponse
    @Value("classpath:testdata/applicationuser/usernewroleresponse.json")
    private Resource userNewRoleResponse;
    private static final Location SAVED_LOCATION = new Location(
            5,
            "Gdansk",
            false
    )

    @WithMockUser(username = "username1", authorities = ["Administrator"])
    def "should get location by id"() {
        when:
        locationService.findById(_) >> Optional.of(SAVED_LOCATION)
        then:
        mvc.perform(MockMvcRequestBuilders
                .get("/admin/5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(convertResourceToString(locationResponse)));
    }

    @WithMockUser(username = "username1", authorities = ["Receptionist"])
    def "should deny access to Receptionist while getting location by id "() {
        when:
        locationService.findById(_) >> Optional.of(SAVED_LOCATION)
        then:
        mvc.perform(MockMvcRequestBuilders
                .get("/admin/5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError())
    }

    @WithMockUser(username = "username1", authorities = ["Administrator"])
    def "should throw No Such Location Exception while getting location by id "() {
        when:
        locationService.findById(_) >> { throw new NoSuchLocationException() }
        then:
        mvc.perform(MockMvcRequestBuilders
                .get("/admin/5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
    }

    @WithMockUser(username = "username1", authorities = ["Administrator"])
    def "should find all locations"() {
        given:
        List<Location> locations = new ArrayList<>();
        locations.add(SAVED_LOCATION);
        locations.add(
                new Location(
                        6,
                        "Sopot",
                        false
                )
        )
        when:
        locationService.findAll() >> locations
        then:
        mvc.perform(MockMvcRequestBuilders
                .get("/admin")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(convertResourceToString(locationsResponse)))
    }

    @WithMockUser(username = "username1", authorities = ["Receptionist"])
    def "should deny access to Receptionist while finding all locations "() {
        when:
        locationService.findAll() >> new ArrayList<>()
        then:
        mvc.perform(MockMvcRequestBuilders
                .get("/admin")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @WithMockUser(username = "username1", authorities = ["Administrator"])
    def "should add location"() {
        when:
        locationService.save(_ as Location) >> SAVED_LOCATION
        then:
        mvc.perform(MockMvcRequestBuilders
                .post("/admin")
                .content(convertResourceToString(locationRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(convertResourceToString(locationResponse)));
    }

    @WithMockUser(username = "username1", authorities = ["Administrator"])
    def "should fail 33 char long name validation while adding location "() {
        when:
        locationService.save(_ as Location) >> { throw new ValidationException() }
        then:
        mvc.perform(MockMvcRequestBuilders
                .post("/admin")
                .content(convertResourceToString(locationRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    def "should forbid adding location without authorization "() {
        when:
        locationService.save(_ as Location) >> SAVED_LOCATION
        then:
        mvc.perform(MockMvcRequestBuilders
                .post("/admin")
                .content(convertResourceToString(locationRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @WithMockUser(username = "username1", authorities = ["Administrator"])
    def "should update location"() {
        when:
        locationService.updateLocation(_ as Location, _ as Long) >> SAVED_LOCATION
        then:
        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/admin/5")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(SAVED_LOCATION));

        mvc.perform(mockRequest)
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(convertResourceToString(locationResponse)))
    }

    @WithMockUser(username = "username1", authorities = ["Administrator"])
    def "should change user's role to receptionist"() {
        setUpLocation()
        setUpUserRole()
        setUpAppUser()
        applicationUser.setApplicationUserRole(
                new ApplicationUserRole(
                        2L,
                        "receptionist",
                        0L
                )
        )
        when:
        applicationUserService.changeUserRoleToReceptionist(_ as Long) >> applicationUser
        then:
        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/admin/4")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(applicationUser));

        mvc.perform(mockRequest)
                .andExpect(status().isOk())
    }

    @WithMockUser(username = "username1", authorities = ["Administrator"])
    def "should delete location by id"() {
        expect:
        mvc.perform(MockMvcRequestBuilders
                .delete("/admin/5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @WithMockUser(username = "username1", authorities = ["Administrator"])
    def "should delete user by id"() {
        given:
        ApplicationUser applicationUser = new ApplicationUser(
                4L,
                "username1",
                "notEncrypted",
                true,
                "email@wp.pl",
                new ApplicationUserRole(
                        1L,
                        "Administrator",
                        0L
                ),
                SAVED_LOCATION
        )
        when:
        applicationUserRepository.findById(_ as Long) >> Optional.of(applicationUser)
        then:
        mvc.perform(MockMvcRequestBuilders
                .delete("/admin/user/4")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
    }
}