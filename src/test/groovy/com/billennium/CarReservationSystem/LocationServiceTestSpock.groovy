package com.billennium.CarReservationSystem

import com.billennium.CarReservationSystem.entity.Location
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchLocationException
import com.billennium.CarReservationSystem.repository.ApplicationUserRepository
import com.billennium.CarReservationSystem.repository.ApplicationUserRoleRepository
import com.billennium.CarReservationSystem.repository.CarRepository
import com.billennium.CarReservationSystem.repository.LocationRepository
import com.billennium.CarReservationSystem.repository.ReservationRepository
import com.billennium.CarReservationSystem.service.ApplicationUserService
import com.billennium.CarReservationSystem.service.CarService
import com.billennium.CarReservationSystem.service.LocationService
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import javax.validation.ConstraintViolation

import static com.billennium.CarReservationSystem.setUp.location
import static com.billennium.CarReservationSystem.setUp.setUpLocation
import static com.billennium.CarReservationSystem.setUp.validator

@SpringBootTest
class LocationServiceTestSpock extends Specification {

    private LocationRepository locationRepository = Mock()
    private CarRepository carRepository = Mock()
    private ApplicationUserRepository applicationUserRepository = Mock()
    private ReservationRepository reservationRepository = Mock()
    private ApplicationUserRoleRepository applicationUserRoleRepository = Mock()
    private CarService carServUnderTest = Mock()
    private ApplicationUserService appuserServUnderTest = Mock()
    private LocationService locServUnderTest = new LocationService(locationRepository, carServUnderTest,
            appuserServUnderTest);

    def setup() {
        setUpLocation()
    }

    def "should save location"() {
        when:
        Location locationSaved = locServUnderTest.save(location)
        then:
        locationSaved.getName() == location.getName()
        1 * locationRepository.save(_ as Location) >> location
    }

    def "should not save location name with invalid validation"() {
        when:
        location.setName("123456789112345678921234567891123")
        Set<ConstraintViolation<Location>> violations = validator.validate(location)
        then:
        violations.size() == 1
    }

    def "should find location by id"() {
        given:
        Long locationId = location.getId()
        when:
        def result = locServUnderTest.findById(locationId)
        then:
        result == Optional.of(location)
        1 * locationRepository.findById(locationId) >> Optional.of(location)
    }

    def "should throw NoSuchLocationException when delete location by id"() {
        given:
        locationRepository.findById(_ as Long) >> { throw new NoSuchLocationException() }
        when:
        locServUnderTest.deleteById(1L)
        then:
        thrown(NoSuchLocationException)
    }

    def "should delete Location by id"() {
        given:
        Long locationId = 5L;
        locationRepository.findById(locationId) >> Optional.of(location)
        when:
        locServUnderTest.findById(locationId).get().setArchived(true)
        then:
        locServUnderTest.findById(locationId).get().isArchived()
    }

    def "should throw NoSuchLocationException during update"() {
        given:
        locationRepository.findById(6L) >> { throw new NoSuchLocationException() }
        when:
        locServUnderTest.updateLocation(location, 6L)
        then:
        thrown(NoSuchLocationException)
    }

    def "should find all locations"() {
        given:
        List<Location> allLocations = new ArrayList<>()
        Location secondLocation = new Location(
                6L,
                "TestingSecondLocation",
                false
        );
        allLocations.add(location)
        allLocations.add(secondLocation)
        locationRepository.findAll() >> allLocations
        when:
        List<Location> allLocationFinalList = locServUnderTest.findAll()
        then:
        allLocations == allLocationFinalList
    }

    def "should update location"() {
        given:
        def beforeUpdate = new Location()
        beforeUpdate.setName("AfterUpdate")
        beforeUpdate.setArchived(false)
        beforeUpdate.setId(5L)
        when:
        Location location2 = locServUnderTest.updateLocation(beforeUpdate, 5L)
        then:
        1 * locationRepository.findById(_ as Long) >> Optional.of(location)
        1 * locationRepository.save(_ as Location) >> location
        location2 == beforeUpdate
    }
}