package com.billennium.CarReservationSystem.security.jwt3;

import com.billennium.CarReservationSystem.entity.ApplicationUser;
import com.billennium.CarReservationSystem.repository.ApplicationUserRepository;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.Optional;

import static com.billennium.CarReservationSystem.setUp.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class UserDetailsServiceImplTest {

    @Mock
    ApplicationUserRepository applicationUserRepository;
    @Mock
    UserDetailsServiceImpl userDetailsServiceImpl;
    @BeforeEach
    public void setUp() {
        userDetailsServiceImpl = new UserDetailsServiceImpl(applicationUserRepository);
        setUpUserRole();
        setUpAppUser();
        setUpLocation();
    }

    @Test
    @SneakyThrows
    void LoadUserByUsername_success() {

        when(applicationUserRepository.findByUsername(anyString())).thenReturn(Optional.of(applicationUser));
        ApplicationUser appUser = applicationUserRepository.findByUsername(applicationUser.getUsername()).orElseThrow();

        UserDetails user = new org.springframework.security.core.userdetails.User(
                appUser.getUsername(), appUser.getPassword(), Collections.singleton(
                new SimpleGrantedAuthority(applicationUser.getApplicationUserRole().getName())));

        assertThat(user.getUsername())
                .isEqualTo(appUser.getUsername());
    }
}