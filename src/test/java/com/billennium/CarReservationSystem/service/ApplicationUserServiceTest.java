package com.billennium.CarReservationSystem.service;

import com.billennium.CarReservationSystem.entity.*;
import com.billennium.CarReservationSystem.entity.dto.UserDTO;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchUserException;
import com.billennium.CarReservationSystem.repository.ApplicationUserRepository;
import com.billennium.CarReservationSystem.repository.ApplicationUserRoleRepository;
import com.billennium.CarReservationSystem.repository.ReservationRepository;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.billennium.CarReservationSystem.setUp.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ApplicationUserServiceTest {

    @Mock
    private ApplicationUserRepository applicationUserRepository;
    @Mock
    private ApplicationUserRoleRepository applicationUserRoleRepository;
    @Mock
    private ReservationRepository reservationRepository;
    private Location location2;
    private ApplicationUser applicationUser2;
    private ApplicationUserService applicationUserService;

    @BeforeEach
    void setUp() {
        applicationUserService = new ApplicationUserService(applicationUserRepository, reservationRepository, applicationUserRoleRepository);
        setUpUserRole();
        setUpLocations();
        setUpAppUser();
        setUpSecondUser();
        setUpReservation();
        setUpCar();
    }

    @Test
    void findAllUsersInGivenLocation_success() {
        long locationId = applicationUser.getLocation().getId();
        List<ApplicationUser> allUsersBeforeStream = new ArrayList<>();

        allUsersBeforeStream.add(applicationUser);
        allUsersBeforeStream.add(applicationUser2);
        when(applicationUserRepository.findAll()).thenReturn(allUsersBeforeStream);
        //when
        List <UserDTO> allUsers = applicationUserService.findAllinGivenLocation(locationId);
        //then
        assertThat(allUsers.size() < 2).isTrue();
        verify(applicationUserRepository).findAll();
    }

    @Test
    void deleteUserById_success() {
        //given
        long userId = applicationUser2.getId();
        when(applicationUserRepository.findById(userId)).thenReturn(Optional.of(applicationUser2));
        when(applicationUserRepository.save(any())).thenReturn(applicationUser2);
        //when
        ApplicationUser applicationUser = applicationUserRepository.findById(userId).get();
        applicationUserService.deleteById(userId);
        //then
        assertThat(applicationUser.isActive()).isFalse();
    }

    @Test
    void deleteUserById_whenUserDoesNotExist_throwNoSuchUserException() {
        Mockito.when(applicationUserRepository.findById(5L)).thenThrow(new NoSuchUserException());
        Assertions.assertThrows(NoSuchUserException.class, () -> {
            applicationUserService.deleteById(5L);
        });
    }

    @Test
    void findAllUserReservations_success() {
        long userId = applicationUser.getId();
        List<Reservation> allReservationsBeforeStream = new ArrayList<>();
        allReservationsBeforeStream.add(reservation);
        when(reservationRepository.findAll()).thenReturn(allReservationsBeforeStream);
        //when
        List<Reservation> allReservations = reservationRepository.findAll().stream()
                .filter(reservation -> reservation.getApplicationUser().getId() == userId)
                .toList();
        //then
        assertThat(allReservations.size() < 2).isTrue();
    }

    @Test
    @SneakyThrows
    void changeUserRoleToReceptionist_success(){
        ApplicationUserRole receptionist = new ApplicationUserRole(
                2L,
                "Receptionist",
                0L
        );
        when(applicationUserRepository.findById(any())).thenReturn(Optional.of(applicationUser));
        when(applicationUserRoleRepository.findById(any())).thenReturn(Optional.of(receptionist));
        Long userId = applicationUser.getId();
        applicationUserService.changeUserRoleToReceptionist(userId);
        assertThat(applicationUser.getApplicationUserRole().getName()).isEqualTo("Receptionist");
    }

    @Test
    @SneakyThrows
    void changeUserRoleToReceptionist_whenUserDoesNotExist_throwNoSuchUserException(){
        Assertions.assertThrows(NoSuchUserException.class, () -> {
            applicationUserService.changeUserRoleToReceptionist(5L);
        });
    }

    private void setUpLocations() {
        setUpLocation();
        location2 = new Location(
                6L,
                "TestingLocation2",
                false
        );
    }

    private void setUpSecondUser(){
        applicationUser2 = new ApplicationUser(
                5L,
                "username2",
                "notEncrypted",
                true,
                "email2@wp.pl",
                applicationUserRole,
                location2
        );
    }
}