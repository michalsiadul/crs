package com.billennium.CarReservationSystem.service;

import com.billennium.CarReservationSystem.email.EmailService;
import com.billennium.CarReservationSystem.entity.*;
import com.billennium.CarReservationSystem.entity.dto.ReservationDTO;
import com.billennium.CarReservationSystem.entity.state.ReservationState;
import com.billennium.CarReservationSystem.exceptions.availability.CarNotAvailableException;
import com.billennium.CarReservationSystem.exceptions.availability.InactiveUserException;
import com.billennium.CarReservationSystem.exceptions.availability.LocationNotAvailableException;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchReservationException;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchUserException;
import com.billennium.CarReservationSystem.repository.*;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.time.LocalDate;
import java.util.*;

import static com.billennium.CarReservationSystem.setUp.*;
import static com.billennium.CarReservationSystem.setUp.applicationUser;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReservationServiceTest {

    @Mock
    private ReservationRepository reservationRepository;
    @Mock
    private ApplicationUserRepository applicationUserRepository;
    @Mock
    private LocationRepository locationRepository;
    @Mock
    private CarRepository carRepository;
    @Mock
    private ApplicationUserRoleRepository applicationUserRoleRepository;
    private LocationService locSerUnderTest;
    private CarService carServUnderTest;
    private ApplicationUserService appuserServUnderTest;
    private ReservationService resSerUnderTest;
    private ApplicationUserRoleService appuserRoleServUnderTest;
    private LocalDate localDate = LocalDate.of(2007,12,03);
    private ReservationState booked = ReservationState.BOOKED;
    private Authentication authentication;
    private SecurityContext securityContext;
    private EmailService mailService;
    private JavaMailSender javaMailSender;
    private MimeMessage mimeMessage;

    @BeforeEach
    @SneakyThrows
    void setUp() {
        mimeMessage = new MimeMessage((Session)null);
        javaMailSender = mock(JavaMailSender.class);
        mailService =new EmailService(javaMailSender);
        carServUnderTest = new CarService(carRepository, locationRepository, reservationRepository);
        appuserServUnderTest = new ApplicationUserService(applicationUserRepository, reservationRepository, applicationUserRoleRepository);
        locSerUnderTest = new LocationService(locationRepository,carServUnderTest,appuserServUnderTest);
        resSerUnderTest=new ReservationService(reservationRepository, locSerUnderTest, carServUnderTest,applicationUserRepository, mailService);
        appuserRoleServUnderTest = new ApplicationUserRoleService(applicationUserRoleRepository);
        setUpUserRole();
        Mockito.when(applicationUserRoleRepository.save(any(ApplicationUserRole.class)))
                .thenReturn(applicationUserRole);
        appuserRoleServUnderTest.save(applicationUserRole);
        setUpLocation();
        Mockito.when(locationRepository.findById(5L))
                .thenReturn(Optional.of(location));
        Mockito.when(locationRepository.save(any(Location.class)))
                .thenReturn(location);
        locSerUnderTest.save(location);
        setUpCar();
        Mockito.when(carRepository.save(any(Car.class)))
                .thenReturn(car);
        carServUnderTest.save(car);
        setUpAppUser();
        setUpAuthentication();
        setUpAppUserDTO();
        setUpReservation();
        securityContext = Mockito.mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    void findAllReservations_success() {
        List<Reservation> allReservations = new ArrayList<>();
        Reservation secondReservation = new Reservation(
                3L,
                location,
                car,
                applicationUser,
                localDate,
                LocalDate.of(2009,12,03),
                booked
        );
        allReservations.add(reservation);
        allReservations.add(secondReservation);
        Mockito.when(reservationRepository.findAll()).thenReturn(allReservations);
        //when
        List<ReservationDTO> allReservationsFinalList = resSerUnderTest.findAll();
        //then
        verify(reservationRepository, times(1)).findAll();
        assertThat(allReservationsFinalList.size()==2).isTrue();
    }

    @Test
    @SneakyThrows
    void addReservationForLoggedInUser_success() {
        when(javaMailSender.createMimeMessage()).thenReturn(mimeMessage);
        whenFindCarByIdThenReturn(car);
        whenFindUserByNameWithAuthThenReturn(applicationUser);
        applicationUser = applicationUserRepository.findByUsername(authentication.getName()).get();
        resSerUnderTest.save(reservation);
        ArgumentCaptor<Reservation> reservationArgumentCaptor = ArgumentCaptor.forClass(Reservation.class);
        verify(reservationRepository).save(reservationArgumentCaptor.capture());
        Reservation capturedLocation = reservationArgumentCaptor.getValue();
        assertThat(capturedLocation).isEqualTo(reservation);
    }

    @Test
    @SneakyThrows
    void addReservationForAnyUser_success() {
        when(javaMailSender.createMimeMessage()).thenReturn(mimeMessage);
        whenFindCarByIdThenReturn(car);
        whenFindUserByIdThenReturn(applicationUser);
        resSerUnderTest.saveForAnyUser(reservation);
        ArgumentCaptor<Reservation> reservationArgumentCaptor = ArgumentCaptor.forClass(Reservation.class);
        verify(reservationRepository).save(reservationArgumentCaptor.capture());
        Reservation capturedLocation = reservationArgumentCaptor.getValue();
        assertThat(capturedLocation).isEqualTo(reservation);
    }

    @Test
    @SneakyThrows
    void reservationSave_whenCarArchivedOrReserved_throwCarNotAvailable() {
        whenFindCarByIdThenReturn(car);
        whenFindUserByNameWithAuthThenReturn(applicationUser);
        applicationUser = applicationUserRepository.findByUsername(authentication.getName()).get();
        Mockito.when(reservationRepository.save(reservation)).thenThrow(new CarNotAvailableException());
        Assertions.assertThrows(CarNotAvailableException.class, () -> {
            resSerUnderTest.save(reservation);
        });
    }

    @Test
    @SneakyThrows
    void reservationSave_whenLocationIsArchived_trowLocationNotAvailableException() {
        whenFindCarByIdThenReturn(car);
        whenFindUserByNameWithAuthThenReturn(applicationUser);
        applicationUser = applicationUserRepository.findByUsername(authentication.getName()).get();
        Mockito.when(reservationRepository.save(reservation)).thenThrow(new LocationNotAvailableException(location));
        Assertions.assertThrows(LocationNotAvailableException.class, () -> {
            resSerUnderTest.save(reservation);
        });
    }

    @Test
    @SneakyThrows
    void reservationSave_whenUserInactive_throwInactiveUserException() {
        whenFindCarByIdThenReturn(car);
        whenFindUserByNameWithAuthThenReturn(applicationUser);
        applicationUser = applicationUserRepository.findByUsername(authentication.getName()).get();
        Mockito.when(reservationRepository.save(reservation)).thenThrow(new InactiveUserException(applicationUser));
        Assertions.assertThrows(InactiveUserException.class, () -> {
            resSerUnderTest.save(reservation);
        });
    }

    @Test
    @SneakyThrows
    void reservationSave_whenUserDoesNotExist_ThrowNoSuchUser() {
        Mockito.when(applicationUserRepository.findByUsername("username1"))
                .thenThrow(new NoSuchUserException());
        Assertions.assertThrows(NoSuchUserException.class, () -> {
            applicationUserRepository.findByUsername("username1");
        });
    }

    @Test
    @SneakyThrows
    void reservationSaveAnyUser_whenCarArchivedOrReserved_ThrowCarNotAvailable() {
        whenFindCarByIdThenReturn(car);
        whenFindUserByIdThenReturn(applicationUser);
        applicationUser = applicationUserRepository.findById(Mockito.any()).orElseThrow();
        Mockito.when(reservationRepository.save(reservation)).thenThrow(new CarNotAvailableException());
        Assertions.assertThrows(CarNotAvailableException.class, () -> {
            resSerUnderTest.saveForAnyUser(reservation);
        });
    }

    @Test
    @SneakyThrows
    void reservationSaveForAnyUser_whenLocationIsArchived_throwLocationNotAvailableException() {
        whenFindCarByIdThenReturn(car);
        whenFindUserByIdThenReturn(applicationUser);
        applicationUser = applicationUserRepository.findById(Mockito.any()).get();
        Mockito.when(reservationRepository.save(reservation)).thenThrow(new LocationNotAvailableException(location));
        Assertions.assertThrows(LocationNotAvailableException.class, () -> {
            resSerUnderTest.saveForAnyUser(reservation);
        });
    }

    @Test
    @SneakyThrows
    void reservationSaveForAnyUser_whenUserIsInactive_throwInactiveUserException() {
        whenFindCarByIdThenReturn(car);
        whenFindUserByIdThenReturn(applicationUser);
        applicationUser = applicationUserRepository.findById(Mockito.any()).orElseThrow();
        Mockito.when(reservationRepository.save(reservation)).thenThrow(new InactiveUserException(applicationUser));
        Assertions.assertThrows(InactiveUserException.class, () -> {
            resSerUnderTest.saveForAnyUser(reservation);
        });
    }

    @Test
    @SneakyThrows
    void reservationSaveForAnyUser_whenUserDoesNotExist_throwNoSuchUser() {
        Mockito.when(applicationUserRepository.findById(Mockito.any()))
                .thenThrow(new NoSuchUserException());
        Assertions.assertThrows(NoSuchUserException.class, () -> {
            applicationUserRepository.findById(Mockito.any());
        });
    }

    @Test
    void findReservationById_success() {
        Long reservationId = reservation.getId();
        whenFindReservationByIdThenReturn(reservation);
        ReservationDTO newReservation = resSerUnderTest.findById(reservationId).get();
        assertThat(reservation.getLocation()).isEqualTo(newReservation.getLocation());
        verify(reservationRepository).findById(reservationId);
    }

    @Test
    void findReservationById_whenReservationDoesNotExist_throwNoSuchReservationException() {
        Long reservationId = reservation.getId();
        Mockito.when(reservationRepository.findById(reservationId)).thenThrow(new NoSuchReservationException());
        Assertions.assertThrows(NoSuchReservationException.class, () -> {
            reservationRepository.findById(reservationId).get();
        });
    }

    @Test
    void deleteReservationById_success() {
        //given
        long reservationId = 2;
        whenFindReservationByIdThenReturn(reservation);
        whenSaveReservationThenReturn(reservation);
        when(carRepository.save(any())).thenReturn(reservation.getCar());
        //when
        resSerUnderTest.deleteById(reservationId);
        System.out.println(reservation.getReservationState());
        //then
        verify(reservationRepository).findById(reservationId);
        assertThat(reservation.getReservationState().toString()).isEqualTo("ARCHIVED");
    }

    @Test
    void deleteReservationById_whenNoSuchReservation_throwNoSuchReservationException() {
        Assertions.assertThrows(NoSuchReservationException.class, () -> {
            resSerUnderTest.deleteById(5l);
        });
    }

    @Test
    @SneakyThrows
    void updateReservation_success(){
        //given
        long reservationId = reservation.getId();
        whenFindCarByIdThenReturn(reservation.getCar());
        whenFindUserByNameThenReturn(applicationUser);
        whenFindUserByNameWithAuthThenReturn(applicationUser);
        applicationUser = applicationUserRepository.findByUsername(authentication.getName()).get();
        whenFindReservationByIdThenReturn(reservation);
        when(javaMailSender.createMimeMessage()).thenReturn(mimeMessage);
        whenSaveReservationThenReturn(reservation);
        Reservation updatedReservation = reservation;
        updatedReservation.setReservationDateStart(LocalDate.of(2000,06,29));
        resSerUnderTest.updateReservation(updatedReservation, reservationId);
        assertEquals(LocalDate.of(2000,06,29), reservation.getReservationDateStart());
    }

    @Test
    void updateReservation_whenReservationDoesNotExist_throwNoSuchReservationException() {
        Assertions.assertThrows(NoSuchReservationException.class, () -> {
            resSerUnderTest.updateReservation(reservation, 5L);
        });
    }

    @Test
    @SneakyThrows
    void updateReservationDateStart_success(){

        Long reservationId = reservation.getId();
        whenFindUserByNameThenReturn(applicationUser);
        applicationUser = applicationUserRepository.findByUsername(authentication.getName()).get();
        Reservation reservationAfterUpdate = new Reservation();
        reservationAfterUpdate.setReservationDateStart(LocalDate.of(2020,11,04));
        whenSaveReservationThenReturn(reservationAfterUpdate);

        when(reservationRepository.findById(any())).thenReturn(Optional.of(reservation));
        when(javaMailSender.createMimeMessage()).thenReturn(mimeMessage);
        resSerUnderTest.updateReservationDateStart(reservationAfterUpdate, reservationId);
        assertThat(localDate).isNotSameAs(reservation.getReservationDateStart());
    }

    @Test
    void changeCarInReservation_success() {
        whenFindUserByNameThenReturn(applicationUser);
        applicationUser = applicationUserRepository.findByUsername(authentication.getName()).get();
        Reservation reservationAfterUpdate = new Reservation();
        reservationAfterUpdate.setCar(new Car(
                5L,
                false,
                "model5",
                0,
                location
        ));
        whenSaveReservationThenReturn(reservationAfterUpdate);
        Reservation actualReservation = reservationRepository.save(reservationAfterUpdate);
        assertThat(car.getModel()).isNotSameAs(actualReservation.getCar().getModel());
        assertThat(actualReservation.getCar().getModel()).isEqualTo("model5");
    }

    @Test
    void changeCarInReservation_whenReservationDoesNotExist_throwNoSuchReservationException() {
        Assertions.assertThrows(NoSuchReservationException.class, () -> {
            resSerUnderTest.changeCarInReservation(reservation, 5L);
        });
    }

    @Test
    @SneakyThrows
    void changeStateToInProgress_success(){
        long reservationId = reservation.getId();
        whenFindReservationByIdThenReturn(reservation);
        Reservation beforeUpdate = reservationRepository.findById(reservationId).orElseThrow(NoSuchElementException::new);
        beforeUpdate.setReservationState(ReservationState.IN_PROGRESS);
        whenSaveReservationThenReturn(beforeUpdate);
        Reservation actualReservation = reservationRepository.save(beforeUpdate);
        assertEquals(ReservationState.IN_PROGRESS, actualReservation.getReservationState());
        assertThat(ReservationState.BOOKED).isNotSameAs(actualReservation.getReservationState());
    }

    @Test
    void changeStateToInProgress_whenReservationDoesNotExist_throwNoSuchReservationException() {
        Assertions.assertThrows(NoSuchReservationException.class, () -> {
            resSerUnderTest.changeStateToInProgress(5L);
        });
    }

    private void setUpAuthentication() {
        authentication = new UsernamePasswordAuthenticationToken(
                applicationUser.getUsername(),
                applicationUser.getPassword(),
                Collections.singleton(
                        new SimpleGrantedAuthority(applicationUserRole.getName())
                ));
    }

    private void whenFindCarByIdThenReturn(Car car){
        Mockito.when(carRepository.findById(any()))
                .thenReturn(Optional.of(car));
    }

    private void whenFindUserByNameWithAuthThenReturn(ApplicationUser applicationUser){
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(applicationUserRepository.findByUsername("username1"))
                .thenReturn(Optional.of(applicationUser));
    }

    private void whenFindUserByNameThenReturn(ApplicationUser applicationUser){
        Mockito.when(applicationUserRepository.findByUsername("username1"))
                .thenReturn(Optional.of(applicationUser));
    }

    private void whenFindUserByIdThenReturn(ApplicationUser applicationUser){
        Mockito.when(applicationUserRepository.findById(Mockito.any()))
                .thenReturn(Optional.of(applicationUser));
    }

    private void whenFindReservationByIdThenReturn(Reservation reservation){
        Mockito.when(reservationRepository.findById(any())).thenReturn(Optional.of(reservation));
    }

    private void whenSaveReservationThenReturn(Reservation reservation){
        Mockito.when(reservationRepository.save(any())).thenReturn(reservation);
    }
}