package com.billennium.CarReservationSystem.service;

import com.billennium.CarReservationSystem.entity.Car;
import com.billennium.CarReservationSystem.entity.Location;
import com.billennium.CarReservationSystem.exceptions.availability.LocationNotAvailableException;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchCarException;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchLocationException;
import com.billennium.CarReservationSystem.repository.CarRepository;
import com.billennium.CarReservationSystem.repository.LocationRepository;
import com.billennium.CarReservationSystem.repository.ReservationRepository;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.*;
import java.util.stream.Collectors;

import static com.billennium.CarReservationSystem.setUp.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CarServiceTest {

    @Mock
    private CarRepository carRepository;
    @Mock
    private LocationRepository locationRepository;
    @Mock
    private ReservationRepository reservationRepository;
    private CarService carServUnderTest;
    private LocationService locServUnderTest;
    private ApplicationUserService appuserSerUnderTest;

    @BeforeEach
    void setUp() {
        carServUnderTest = new CarService(carRepository, locationRepository, reservationRepository);
        locServUnderTest = new LocationService(locationRepository, carServUnderTest, appuserSerUnderTest);
        setUpLocation();
        when(locationRepository.save(any(Location.class))).thenReturn(location);
        locServUnderTest.save(location);
        setUpCar();
    }

    @Test
    void addCar_carModelName33charLong_ValidationFailed() {
        car.setModel("123456789112345678921234567891123");
        Set<ConstraintViolation<Car>> violations = validator.validate(car);
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void addCar_success() {
        //given
        whenFindLocationByIdThenReturn(location);
        whenSaveCarThenReturn(car);
        //when
        carServUnderTest.save(car);
        //then
        ArgumentCaptor<Car> carArgumentCaptor = ArgumentCaptor.forClass(Car.class);
        verify(carRepository).save(carArgumentCaptor.capture());
        Car capturedCar = carArgumentCaptor.getValue();
        assertThat(capturedCar).isEqualTo(car);
    }

    @Test
    @SneakyThrows
    void saveCar_whenLocationDoesNotExist_throwNoSuchLocationException() {
        when(locationRepository.findById(any())).thenThrow(new NoSuchLocationException());
        Assertions.assertThrows(NoSuchLocationException.class, () -> {
            carServUnderTest.save(car);
        });
    }

    @Test
    @SneakyThrows
    void saveCar_whenLocationIsArchived_throwLocationNotAvailableException() {
        whenFindLocationByIdThenReturn(location);
        location.setArchived(true);
        locServUnderTest.save(location);
        Assertions.assertThrows(LocationNotAvailableException.class, () -> {
            carServUnderTest.save(car);
        });
    }

    @Test
    void findCarById_success() {
        //given
        Long carId = car.getId();
        //when
        when(carRepository.findById(carId)).thenReturn(Optional.of(car));
        //then
        assertThat(carServUnderTest.findById(carId))
                .isEqualTo(Optional.of(car));
        //verify that repository was called
        verify(carRepository, times(1)).findById(carId);
    }

    @Test
    void findCarById_whenCarDoesNotExist_throwNoSuchCarException() {
        when(carRepository.findById(5L)).thenThrow(new NoSuchCarException());
        Assertions.assertThrows(NoSuchCarException.class, () -> {
            carServUnderTest.findById(5L);
        });
    }

    @Test
    void updateCar_success() {
        //given
        when(carServUnderTest.findById(Mockito.any())).thenReturn(Optional.of(car));
        Car carAfterUpdate = new Car();
        carAfterUpdate.setKilometers(1);
        carAfterUpdate.setLocation(location);
        carAfterUpdate.setModel("AfterUpdate");
        carAfterUpdate.setArchived(false);
        carAfterUpdate.setId(4L);
        whenSaveCarThenReturn(carAfterUpdate);
        whenFindLocationByIdThenReturn(location);
        Car actualCar = carServUnderTest.updateCar(car, 4L);
                //.getBody();
        assertEquals("AfterUpdate", actualCar.getModel());
    }

    @Test
    void updateCar_whenCarDoesNotExist_throwNoSuchCarException() {
        Assertions.assertThrows(NoSuchCarException.class, () -> {
            carServUnderTest.updateCar(car, 4L);
        });
    }

    @Test
    void updateCarKilometers_success() {
        //given
        when(carServUnderTest.findById(Mockito.any())).thenReturn(Optional.of(car));
        int km = 20;
        int oldKm = car.getKilometers();
        Long carId =car.getId();
        whenFindLocationByIdThenReturn(location);
        //when
        carServUnderTest.updateCarKilometers(km, carId);
        //then:
        verify(carRepository).findById(Mockito.any());
        verify(carRepository).save(any(Car.class));
        verify(locationRepository).findById(5L);
        assertEquals(20, car.getKilometers());
        assertThat(car.getKilometers()).isNotSameAs(oldKm);
    }

    @Test
    @SneakyThrows
    void updateCarKilometers_whenCarDoesNotExist_throwsNoSuchCarException() {
        Assertions.assertThrows(NoSuchCarException.class, () -> {
            carServUnderTest.updateCarKilometers(20, 4L);
        });
    }

    @Test
    void deleteCarById_success() {
        //given
        Long carId = 4L;
        when(carRepository.findById(carId)).thenReturn(Optional.ofNullable(car));
        //when
        carServUnderTest.deleteById(carId);
        //then
        verify(carRepository).findById(carId);
        boolean assertion = carServUnderTest.findById(carId).get().isArchived();
        assertThat(assertion).isTrue();
    }

    @Test
    void deleteCarById_whenCarDoesNotExist_throwsNoSuchCarException() {
        when(carServUnderTest.findById(5L)).thenThrow(new NoSuchCarException());
        Assertions.assertThrows(NoSuchCarException.class, () -> {
            carServUnderTest.deleteById(5L);
        });
    }

    @Test
    void findAllCarsInGivenLocation_success() {
        //given
        long locationId = 5;
        List<Car> allCarsBeforeStream = new ArrayList<>();
        Location secondLocation = new Location(
                6L,
                "TestingSecondLocation",
                false
        );
        allCarsBeforeStream.add(car);
        allCarsBeforeStream.add(new Car(
                5L,
                false,
                "model5",
                0,
                secondLocation
        ));
        when(carRepository.findAll()).thenReturn(allCarsBeforeStream);
        //when
        List<Car> allCars = carServUnderTest.findAllInGivenLocation(locationId);
        //then
        verify(carRepository, times (1)).findAll();
        assertThat(allCars.size()<2).isTrue();
    }
    @Test
    void findAllCars_success() {
        List<Car> allCars = new ArrayList<>();
        Location secondLocation = new Location(
                6L,
                "TestingSecondLocation",
                false
        );
        allCars.add(car);
        allCars.add(new Car(
                5L,
                false,
                "model5",
                0,
                secondLocation
        ));
        when(carRepository.findAll()).thenReturn(allCars);
        //when
        List<Car> allCarsFinalList = carServUnderTest.findAll();
        //then
        verify(carRepository, times (1)).findAll();
        assertEquals(allCars,allCarsFinalList);
    }

    private void whenFindLocationByIdThenReturn(Location location){
        when(locationRepository.findById(Mockito.any())).thenReturn(Optional.of(location));
    }

    private void whenSaveCarThenReturn(Car car){
        when(carRepository.save(any(Car.class))).thenReturn(car);
    }
}