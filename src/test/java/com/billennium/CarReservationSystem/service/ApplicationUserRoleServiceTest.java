package com.billennium.CarReservationSystem.service;

import com.billennium.CarReservationSystem.entity.ApplicationUserRole;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchUserException;
import com.billennium.CarReservationSystem.repository.ApplicationUserRoleRepository;
import com.billennium.CarReservationSystem.setUp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.billennium.CarReservationSystem.setUp.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ApplicationUserRoleServiceTest {

    @Mock
    private ApplicationUserRoleRepository applicationUserRoleRepository;
    private ApplicationUserRoleService appuserServUnderTest;

    @BeforeEach
    void setUp() {
        appuserServUnderTest= new ApplicationUserRoleService(applicationUserRoleRepository);
        setUpUserRole();
    }

    @Test
    void saveRole_success() {
        Mockito.when(applicationUserRoleRepository.save(any(ApplicationUserRole.class)))
                .thenReturn(applicationUserRole);
        //when
        appuserServUnderTest.save(applicationUserRole);
        //then
        ArgumentCaptor<ApplicationUserRole> locationArgumentCaptor
                = ArgumentCaptor.forClass(ApplicationUserRole.class);
        verify(applicationUserRoleRepository).save(locationArgumentCaptor.capture());
        ApplicationUserRole capturedApplicationUserRole = locationArgumentCaptor.getValue();
        assertThat(capturedApplicationUserRole).isEqualTo(applicationUserRole);
    }


    @Test
    void findRoleById_success() {
        //given
        Long roleId = applicationUserRole.getId();
        //when
        Mockito.when(applicationUserRoleRepository.findById(roleId))
                .thenReturn(Optional.of(applicationUserRole));
        //then
        assertThat(appuserServUnderTest.findById(roleId))
                .isEqualTo(Optional.of(applicationUserRole));
        //verify that repository was called
        verify(applicationUserRoleRepository, times(1)).findById(roleId);
    }

    @Test
    void findRoleById_whenUserDoesNotExist_throwsNoSuchUserException() {
        Mockito.when(applicationUserRoleRepository.findById(Mockito.any())).thenThrow(new NoSuchUserException());
        Assertions.assertThrows(NoSuchUserException.class, () -> {
            appuserServUnderTest.findById(5L);
        });
    }
}