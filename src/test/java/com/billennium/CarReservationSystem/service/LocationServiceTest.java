package com.billennium.CarReservationSystem.service;

import com.billennium.CarReservationSystem.entity.Location;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchLocationException;
import com.billennium.CarReservationSystem.repository.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintViolation;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.billennium.CarReservationSystem.setUp.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class LocationServiceTest {

    @Mock
    private LocationRepository locationRepository;
    @Mock
    private CarRepository carRepository;
    @Mock
    private ApplicationUserRepository applicationUserRepository;
    @Mock
    private ReservationRepository reservationRepository;
    @Mock
    private ApplicationUserRoleRepository applicationUserRoleRepository;
    private LocationService locServUnderTest;
    private CarService carServUnderTest;
    private ApplicationUserService appuserServUnderTest;

    @BeforeEach
    void setUp() {
        carServUnderTest = new CarService(carRepository, locationRepository, reservationRepository);
        appuserServUnderTest = new ApplicationUserService(applicationUserRepository, reservationRepository, applicationUserRoleRepository);
        locServUnderTest = new LocationService(locationRepository, carServUnderTest, appuserServUnderTest);
        setUpLocation();
    }

    @Test
    void saveLocation_success() {
        //given
        Mockito.when(locationRepository.save(any(Location.class))).thenReturn(location);
        //when
        locServUnderTest.save(location);
        //then
        ArgumentCaptor<Location> locationArgumentCaptor = ArgumentCaptor.forClass(Location.class);
        verify(locationRepository).save(locationArgumentCaptor.capture());
        Location capturedLocation = locationArgumentCaptor.getValue();
        assertThat(capturedLocation).isEqualTo(location);
    }

    @Test
    void saveLocation_locationName33charLong_ValidationFailed() {
        location.setName("123456789112345678921234567891123");
        Set<ConstraintViolation<Location>> violations = validator.validate(location);
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void findLocationById_success() {
        //given
        Long locationId = location.getId();
        //when
        Mockito.when(locationRepository.findById(locationId)).thenReturn(Optional.of(location));
        //then
        assertThat(locServUnderTest.findById(locationId))
                .isEqualTo(Optional.of(location));
        //verify that repository was called
        verify(locationRepository, times(1)).findById(locationId);
    }

    @Test
    void deleteLocationById_success() {
        //given
        Long locationId = 5L;
        Mockito.when(locationRepository.findById(locationId)).thenReturn(Optional.ofNullable(location));
        Location toArchived =
                locServUnderTest.findById(locationId).orElseThrow(NoSuchLocationException::new);
        //when
        toArchived.setArchived(true);
        //then
        verify(locationRepository).findById(locationId);
       assertThat(
               locServUnderTest.findById(locationId).get().isArchived()
       ).isTrue();
    }

    @Test
    void deleteLocationById_whenLocationDoesNotExist_throwNoSuchLocationException() {
        Assertions.assertThrows(NoSuchLocationException.class, () -> {
            locServUnderTest.deleteById(6L);
        });
    }

    @Test
    void updateLocation_success() {
        //given
        Mockito.when(locationRepository.findById(5L)).thenReturn(Optional.of(location));
        Location locationAfterUpdate = new Location();
        locationAfterUpdate.setArchived(false);
        locationAfterUpdate.setName("AfterUpdate");
        locationAfterUpdate.setId(5L);
        Mockito.when(locServUnderTest.save(any(Location.class))).thenReturn(locationAfterUpdate);
        Location actualLocation = locServUnderTest.updateLocation(locationAfterUpdate, 5L);
        assertEquals("AfterUpdate", actualLocation.getName());
    }

    @Test
    void updateLocation_whenLocationDoesNotExist_throwNoSuchLocationException() {
        Assertions.assertThrows(NoSuchLocationException.class, () -> {
            locServUnderTest.updateLocation(location, 6L);
        });
    }
    @Test
    void findAllLocations_success() {
        List<Location> allLocations = new ArrayList<>();
        Location secondLocation = new Location(
                6L,
                "TestingSecondLocation",
                false
        );
        allLocations.add(location);
        allLocations.add(secondLocation);
        Mockito.when(locationRepository.findAll()).thenReturn(allLocations);
        //when
        List<Location> allLocationsFinalList = locServUnderTest.findAll();
        //then
        verify(locationRepository, times (1)).findAll();
        assertEquals(allLocations,allLocationsFinalList);
    }
}