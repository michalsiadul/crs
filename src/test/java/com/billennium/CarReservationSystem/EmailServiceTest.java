package com.billennium.CarReservationSystem;

import com.billennium.CarReservationSystem.email.EmailService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmailServiceTest {

    @Mock
    private EmailService emailServiceImpl;
    @Mock
    private JavaMailSender javaMailSender;
    @Mock
    private MimeMessage mimeMessage;

    @BeforeEach
    public void setUp() {
        mimeMessage = new MimeMessage((Session)null);
    }

    @Test
    @SneakyThrows
    void emailTest() {
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setTo("example@example.pl");
        mimeMessageHelper.setSubject("test");
        mimeMessageHelper.setText("test");
        String message = "fail";
        javaMailSender.send(mimeMessage);
        assertEquals("example@example.pl", mimeMessage.getRecipients(MimeMessage.RecipientType.TO)[0].toString());
    }
}
