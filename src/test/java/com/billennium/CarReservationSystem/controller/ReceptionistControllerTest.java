package com.billennium.CarReservationSystem.controller;

import com.billennium.CarReservationSystem.entity.*;
import com.billennium.CarReservationSystem.entity.dto.ReservationDTO;
import com.billennium.CarReservationSystem.entity.dto.UserDTO;
import com.billennium.CarReservationSystem.entity.state.ReservationState;
import com.billennium.CarReservationSystem.repository.ReservationRepository;
import com.billennium.CarReservationSystem.service.CarService;
import com.billennium.CarReservationSystem.service.ReservationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.billennium.CarReservationSystem.setUp.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
class ReceptionistControllerTest {

    @MockBean
    private CarService carService;
    @MockBean
    private ReservationService reservationService;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ReceptionistController receptionistController;
    @Mock
    private ReservationRepository reservationRepository;
    @Value("classpath:testdata/car/carRequest.json")
    private Resource carRequest;
    @Value("classpath:testdata/car/carResponse.json")
    private Resource carResponse;
    @Value("classpath:testdata/reservation/reservationRequest.json")
    private Resource reservationRequest;
    @Value("classpath:testdata/reservation/reservationResponse.json")
    private Resource reservationResponse;
    @Autowired
    ObjectMapper mapper;

    @BeforeEach
    void setUp() {
        setUpUserRole();
        setUpAppUser();
        setUpLocation();
        setUpCar();
        setUpReservation();
        setUpAppUserDTO();
        setUpReservationDTO();
    }

    @Test
    @WithMockUser(username = "username1", authorities= { "Receptionist"})
    void findAllReservations_success() throws Exception {
        List<ReservationDTO> reservations = new ArrayList<>();
        reservations.add(new ReservationDTO());
        reservations.add(new ReservationDTO(
                2L,
                new Location(
                        8L,
                        "LocationOfReservation",
                        false
                ),
                new Car(),
                new UserDTO(),
                LocalDate.of(2008,12,03),
                LocalDate.of(2009,12,03),
                ReservationState.BOOKED
        ));

        Mockito.when(reservationService.findAll()).thenReturn(reservations);

        mvc.perform(MockMvcRequestBuilders
                        .get("/receptionist")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[1].location.name", is("LocationOfReservation")));
    }

    @Test
    @SneakyThrows
    void findAllReservations_withoutAuth_forbidden() {

        Mockito.when(reservationService.findAll()).thenReturn(new ArrayList<>());

        mvc.perform(MockMvcRequestBuilders
                        .get("/receptionist")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Receptionist"})
    void findAllCarsInGivenLocation_success() {
        List<Car> cars = new ArrayList<>();
        cars.add(car);
        cars.add(new Car(
                        5L,
                        false,
                        "model5",
                        0,
                        new Location(
                                5L,
                                "TestingLocation",
                                false
                        )
                )
        );

        Mockito.when(carService.findAllInGivenLocation(Mockito.any())).thenReturn(cars);

        mvc.perform(MockMvcRequestBuilders
                        .get("/receptionist/cars/5")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[1].model", is("model5")));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Employee"})
    void findAllCarsInGivenLocation_EmployeeAttempt_AccessDenied() {

        Mockito.when(carService.findAllInGivenLocation(Mockito.any())).thenReturn(new ArrayList<>());

        mvc.perform(MockMvcRequestBuilders
                        .get("/receptionist/cars/5")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Receptionist"})
    void findOneReservationById_success() {

         Mockito.when(reservationService.findById(Mockito.any())).thenReturn(Optional.of(reservationDTO));

        mvc.perform(MockMvcRequestBuilders
                .get("/receptionist/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.reservationState", is("BOOKED")));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Receptionist"})
    void findOneCarById_success() {

         Mockito.when(carService.findById(Mockito.any())).thenReturn(Optional.of(car));

        mvc.perform(MockMvcRequestBuilders
                .get("/receptionist/car/4")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.model", is("model4")));
    }

    @Test
    @WithMockUser(username = "username1", authorities= { "Receptionist"})
    void findAllCars_success() throws Exception {
        List<Car> cars = new ArrayList<>();
        cars.add(car);
        cars.add(new Car(
                        5L,
                        false,
                        "model5",
                        0,
                        new Location(
                                5L,
                                "TestingLocation",
                                false
                        )
                )
        );

        Mockito.when(carService.findAll()).thenReturn(cars);

        mvc.perform(MockMvcRequestBuilders
                .get("/receptionist/cars")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[1].model", is("model5")));
    }

    @Test
    @WithMockUser(username = "username1", authorities= { "Receptionist"})
    void addCar_success() throws Exception {
        Mockito.when(carService.save(any(Car.class))).thenReturn(car);
        mvc.perform(MockMvcRequestBuilders
                        .post("/receptionist")
                        .content(convertResourceToString(carRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(convertResourceToString(carResponse)));
    }

    @Test
    @WithMockUser(username = "username1", authorities= { "Receptionist"})
    void addReservationForAnyUser_success() throws Exception {
        Mockito.when(reservationService.saveForAnyUser(Mockito.any())).thenReturn(reservationDTO);

        mvc.perform(MockMvcRequestBuilders
                        .post("/receptionist/reservation")
                        .content(convertResourceToString(reservationRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(convertResourceToString(reservationResponse)));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Receptionist"})
    void deleteCarById_success() {
        mvc.perform(MockMvcRequestBuilders
                        .delete("/receptionist/4")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Receptionist"})
    void updateCar_success() {
        Mockito.when(carService.updateCar(Mockito.any(Car.class), Mockito.any())).thenReturn(car);
        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/receptionist/4")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(car));

        mvc.perform(mockRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.model", is("model4")));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Receptionist"})
    void changeReservationStateToInProgress_success() {
        Mockito.when(reservationService.changeStateToInProgress(Mockito.any())).thenReturn(reservationDTO);
        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/receptionist/state/2")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(reservationDTO));

        mvc.perform(mockRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Receptionist"})
    void updateCarKilometers_success() {
        Mockito.when(carService.updateCarKilometers(20, 4L)).thenReturn(car);
        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/receptionist/4?km=20")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(car));

        mvc.perform(mockRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()));
    }
}