package com.billennium.CarReservationSystem.controller;

import com.billennium.CarReservationSystem.entity.ApplicationUser;
import com.billennium.CarReservationSystem.entity.ApplicationUserRole;
import com.billennium.CarReservationSystem.entity.Location;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchLocationException;
import com.billennium.CarReservationSystem.repository.ApplicationUserRepository;
import com.billennium.CarReservationSystem.repository.LocationRepository;
import com.billennium.CarReservationSystem.service.ApplicationUserService;
import com.billennium.CarReservationSystem.service.LocationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.billennium.CarReservationSystem.setUp.*;
import static org.hamcrest.Matchers.*;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
class AdministratorControllerTest {

    //MethodName_StateUnderTest_ExpectedBehavior

    private static final Location SAVED_LOCATION = new Location(
            5,
            "Gdansk",
            false
    );
    @MockBean
    private LocationService locationService;
    @MockBean
    private LocationRepository locationRepository;
    @MockBean
    private ApplicationUserRepository applicationUserRepository;
    @MockBean
    private ApplicationUserService applicationUserService;
    @Autowired
    private MockMvc mvc;
    @Autowired
    ObjectMapper mapper;
    @Autowired
    private AdministratorController adminController;
    @Value("classpath:testdata/location/locationRequest.json")
    private Resource locationRequest;
    @Value("classpath:testdata/location/locationResponse.json")
    private Resource locationResponse;

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Administrator"})
    void getLocationById_success(){
        Mockito.when(locationService.findById(Mockito.any())).thenReturn(Optional.of(SAVED_LOCATION));

        mvc.perform(MockMvcRequestBuilders
                .get("/admin/5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.name", is("Gdansk")));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Receptionist"})
    void getLocationById_ReceptionistAttempt_AccessDenied(){
        Mockito.when(locationService.findById(Mockito.any())).thenReturn(Optional.of(SAVED_LOCATION));

        mvc.perform(MockMvcRequestBuilders
                        .get("/admin/5")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Administrator"})
    void getLocationById_whenLocationDoesNotExist_throwNoSuchLocationException(){
        Mockito.when(locationService.findById(Mockito.any())).thenThrow(new NoSuchLocationException());

        mvc.perform(MockMvcRequestBuilders
                        .get("/admin/5")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Administrator"})
    void findAllLocations_success() {
        List<Location> locations = new ArrayList<>();
        locations.add(SAVED_LOCATION);
        locations.add(
                new Location(
                        6,
                        "Sopot",
                        false
                )
        );
        Mockito.when(locationService.findAll()).thenReturn(locations);

        mvc.perform(MockMvcRequestBuilders
                        .get("/admin")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[1].name", is("Sopot")));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Receptionist"})
    void findAllLocations_ReceptionistAttempt_AccessDenied() {

        Mockito.when(locationService.findAll()).thenReturn((new ArrayList<>()));

        mvc.perform(MockMvcRequestBuilders
                        .get("/admin")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Administrator"})
    void addLocation_success() {
        Mockito.when(locationService.save(Mockito.any(Location.class))).thenReturn(SAVED_LOCATION);
        mvc.perform(MockMvcRequestBuilders
                        .post("/admin")
                        .content(convertResourceToString(locationRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(convertResourceToString(locationResponse)));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Administrator"})
    void addLocation_locationName33charLong_ValidationFailed() {

        Mockito.when(locationService.save(Mockito.any(Location.class))).thenThrow(new ValidationException());

        mvc.perform(MockMvcRequestBuilders
                        .post("/admin")
                        .content(convertResourceToString(locationRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    @SneakyThrows
    void addLocationAttempt_withoutAuthorization_forbidden() {
        Mockito.when(locationService.save(Mockito.any(Location.class))).thenReturn(SAVED_LOCATION);

        mvc.perform(MockMvcRequestBuilders
                        .post("/admin")
                        .content(convertResourceToString(locationRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Administrator"})
    void updateLocation_success() {
        Mockito.when(locationService.updateLocation(Mockito.any(Location.class), Mockito.any())).thenReturn(SAVED_LOCATION);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/admin/5")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(SAVED_LOCATION));

        mvc.perform(mockRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.name", is("Gdansk")));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Administrator"})
    void changeUserRole_ToReceptionist_success() {
        setUpLocation();
        setUpUserRole();
        setUpAppUser();
        applicationUser.setApplicationUserRole(
                new ApplicationUserRole(
                        2L,
                        "receptionist",
                        0L
                )
        );
        Mockito.when(applicationUserService
                .changeUserRoleToReceptionist(
                        Mockito.any())).thenReturn(applicationUser);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/admin/4")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(applicationUser));

        mvc.perform(mockRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.applicationUserRole.name", is("receptionist")));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Administrator"})
    void deleteLocationById_success() {

        mvc.perform(MockMvcRequestBuilders
                .delete("/admin/5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Administrator"})
    void deleteUserById_success() {

       ApplicationUser applicationUser =  new ApplicationUser(
                4L,
                "username1",
                "notEncrypted",
                true,
                "email@wp.pl",
               new ApplicationUserRole(
                       1L,
                       "Administrator",
                       0L
               ),
                SAVED_LOCATION
        );

        Mockito.when(applicationUserRepository.findById(Mockito.any())).thenReturn(Optional.of(applicationUser));

        mvc.perform(MockMvcRequestBuilders
                .delete("/admin/user/4")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }
}