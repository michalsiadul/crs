package com.billennium.CarReservationSystem.controller;

import com.billennium.CarReservationSystem.entity.*;
import com.billennium.CarReservationSystem.entity.dto.ReservationDTO;
import com.billennium.CarReservationSystem.entity.state.ReservationState;
import com.billennium.CarReservationSystem.service.CarService;
import com.billennium.CarReservationSystem.service.LocationService;
import com.billennium.CarReservationSystem.service.ReservationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static com.billennium.CarReservationSystem.setUp.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.time.LocalDate;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
class EmployeeControllerTest {

    @MockBean
    private ReservationService reservationService;
    @MockBean
    private CarService carService;
    @MockBean
    private LocationService locationService;
    @Autowired
    ObjectMapper mapper;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private EmployeeController employeeController;
    @Value("classpath:testdata/reservation/reservationRequest.json")
    private Resource reservationRequest;
    @Value("classpath:testdata/reservation/reservationResponse.json")
    private Resource reservationResponse;

    @BeforeEach
    void setUp() {
        setUpUserRole();
        setUpLocation();
        setUpAppUser();
        setUpCar();
        setUpReservation();
        setUpAppUserDTO();
        setUpReservationDTO();
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Employee"})
    void addReservation_success() {

        Mockito.when(reservationService.save(Mockito.any())).thenReturn(reservationDTO);

        mvc.perform(MockMvcRequestBuilders
                        .post("/employee")
                        .content(convertResourceToString(reservationRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(convertResourceToString(reservationResponse)));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Employee"})
    void updateReservation_success() {
        ReservationDTO reservationDTOupdate = new ReservationDTO(
                2L,
                location,
                car,
                userDTO,
                LocalDate.of(2007,12,03),
                LocalDate.of(2008,12,03),
                ReservationState.BOOKED
        );

        Mockito.when(reservationService.updateReservation(Mockito.any(Reservation.class), Mockito.any())).thenReturn(reservationDTOupdate);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/employee/2")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(reservationDTOupdate));

        mvc.perform(mockRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.reservationState", is("BOOKED")));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Employee"})
    void updateReservationDateStart_success() {

        Mockito.when(reservationService.updateReservationDateStart(Mockito.any(Reservation.class), Mockito.any())).thenReturn(reservation);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/employee/datestart/2")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(reservation));

        mvc.perform(mockRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.reservationState", is("BOOKED")));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Employee"})
    void updateReservationDateEnd_success() {

        Mockito.when(reservationService.updateReservationDateEnd(Mockito.any(Reservation.class), Mockito.any())).thenReturn(reservation);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/employee/dateend/2")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(reservation));

        mvc.perform(mockRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.reservationState", is("BOOKED")));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Employee"})
    void changeCarInReservation_success() {
        Mockito.when(reservationService.changeCarInReservation(Mockito.any(Reservation.class), Mockito.any())).thenReturn(reservationDTO);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/employee/car/2")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(reservationDTO));

        mvc.perform(mockRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.car.model", is("model4")));
    }

    @Test
    @SneakyThrows
    @WithMockUser(username = "username1", authorities= { "Employee"})
    void deleteReservationById_success() {
        mvc.perform(MockMvcRequestBuilders
                        .delete("/employee/2")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}