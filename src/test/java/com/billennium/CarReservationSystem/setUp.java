package com.billennium.CarReservationSystem;

import com.billennium.CarReservationSystem.entity.*;
import com.billennium.CarReservationSystem.entity.dto.ReservationDTO;
import com.billennium.CarReservationSystem.entity.dto.UserDTO;
import com.billennium.CarReservationSystem.entity.state.ReservationState;
import lombok.SneakyThrows;
import org.springframework.core.io.Resource;
import org.springframework.util.StreamUtils;
import spock.lang.Shared;

import javax.validation.Validation;
import javax.validation.Validator;
import java.nio.charset.Charset;
import java.time.LocalDate;


public class setUp {

    public static Reservation reservation;
    public static ApplicationUserRole applicationUserRole;
    public static Location location;
    public static Car car;
    public static ApplicationUser applicationUser;
    public static UserDTO userDTO;
    public static ReservationDTO reservationDTO;
    public static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public static void setUpReservation(){
        reservation = new Reservation(
                2L,
                location,
                car,
                applicationUser,
                LocalDate.of(2001,06,29),
                LocalDate.of(2002,06,29),
                ReservationState.BOOKED
        );
    }
    public static void setUpUserRole(){
        applicationUserRole = new ApplicationUserRole(
                1L,
                "Administrator",
                0L
        );
    }

    public static void setUpLocation(){
        location = new Location(
                5L,
                "TestingLocation",
                false
        );
    }

    public static void setUpCar(){
        car = new Car(
                4L,
                false,
                "model4",
                0,
                location
        );
    }

    public static void setUpAppUser() {
        applicationUser = new ApplicationUser(
                4L,
                "username1",
                "notEncrypted",
                true,
                "email@wp.pl",
                applicationUserRole,
                location
        );
    }


    public static void setUpReservationDTO(){
        reservationDTO = new ReservationDTO(
                2L,
                location,
                car,
                userDTO,
                LocalDate.of(2001,06,29),
                LocalDate.of(2002,06,29),
                ReservationState.BOOKED
        );
    }

    public static void setUpAppUserDTO(){
        userDTO = new UserDTO(
                4L,
                "username1",
                true,
                "email@wp.pl",
                applicationUserRole,
                location
        );
    }

    @SneakyThrows
    public static String convertResourceToString(Resource resource) {
        return StreamUtils.copyToString(resource.getInputStream(), Charset.defaultCharset());
    }
}
