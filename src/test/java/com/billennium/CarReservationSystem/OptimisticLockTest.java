package com.billennium.CarReservationSystem;

import com.billennium.CarReservationSystem.entity.ApplicationUserRole;
import com.billennium.CarReservationSystem.repository.ApplicationUserRoleRepository;
import com.billennium.CarReservationSystem.service.ApplicationUserRoleService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
class OptimisticLockTest {

    @Autowired
    private ApplicationUserRoleRepository applicationUserRoleRepository;
    @SpyBean
    private ApplicationUserRoleService appuserServUnderTest;
    private final List<String> roleNames = Arrays.asList("111", "222");

    @RepeatedTest(10)
    @Disabled
    @SneakyThrows
    //todo: wątki
    void shouldChangeName_withOptLock() throws InterruptedException {
        //given
        ApplicationUserRole roleA = applicationUserRoleRepository.save(
                new ApplicationUserRole(
                        4,
                        "Sth",
                        0L
                )
        );
         assertEquals(0, roleA.getVersion());
        //when
        final ExecutorService executor = Executors.newFixedThreadPool(roleNames.size());
        for (final String roleName : roleNames) {
            executor.execute(() -> appuserServUnderTest.updateRoleNameWithoutWait(roleA.getId(), roleName));
        }
        executor.shutdown();
        assertTrue(executor.awaitTermination(1, TimeUnit.MINUTES));
        final ApplicationUserRole finalRole = applicationUserRoleRepository.findById(
                roleA.getId()).orElseThrow(() -> new IllegalArgumentException("No role name found!"));
        assertAll(
                () -> assertEquals(1, finalRole.getVersion()),
                () -> assertEquals("111", finalRole.getName()),
                () -> verify(appuserServUnderTest, times(2))
                        .updateRoleNameWithoutWait(any(), any())
        );
    }

    @Test
    void concurrencyTest() throws InterruptedException {
        ApplicationUserRole roleA = applicationUserRoleRepository.save(
                new ApplicationUserRole(
                        4,
                        "Sth",
                        0L
                )
        );
        assertEquals(0, roleA.getVersion());
        appuserServUnderTest.updateRoleNameWithWait(roleA.getId(), "111");
        appuserServUnderTest.updateRoleNameWithoutWait(roleA.getId(), "222");
        final ApplicationUserRole finalRole = applicationUserRoleRepository.findById(
                roleA.getId()).orElseThrow(() -> new IllegalArgumentException("No role name found!"));

        assertAll(
                () -> assertEquals(2, finalRole.getVersion()),
                () -> assertEquals("222", finalRole.getName()),
                () -> verify(appuserServUnderTest, times(1))
                        .updateRoleNameWithoutWait(any(), any()),
                () -> verify(appuserServUnderTest, times(1))
                .updateRoleNameWithoutWait(any(), any())
        );
    }
}