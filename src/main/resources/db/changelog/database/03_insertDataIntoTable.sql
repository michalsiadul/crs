--liquibase formatted sql
--changeset msiadul:2
INSERT INTO application_user_role (id, name)
VALUES (1, 'Employee');

INSERT INTO application_user_role (id, name)
VALUES (2, 'Receptionist');

INSERT INTO application_user_role (id, name)
VALUES (3, 'Administrator');

INSERT INTO location (id, name, archived)
VALUES (1, 'Lodz', false);

INSERT INTO location (id, name, archived)
VALUES (2, 'Warsaw', false);

INSERT INTO location (id, name, archived)
VALUES (3, 'Poznan', false);

INSERT INTO location (id, name, archived)
VALUES (4, 'Katowice', false);

INSERT INTO car (id, kilometers, model, location_id, archived)
VALUES (1, 0, 'model1', 1, false);

INSERT INTO car (id, kilometers, model, location_id, archived)
VALUES (2, 0, 'model2', 1, false);

INSERT INTO car (id, kilometers, model, location_id, archived)
VALUES (3, 0, 'model3', 2, false);

INSERT INTO application_user (
id, active, email, password, username, application_user_role_id, location_id)
VALUES (
1, true, 'idehjadhajkdawkdh@gmail.com', '$2a$10$qW.LwKfTSJkrq79cFPEz8OTYSa3sCKJUy2fxTIw3k7y71Wj7qN.bi', 'employee', 1, 1);

INSERT INTO application_user (
id, active, email, password, username, application_user_role_id, location_id)
VALUES (
2, true, 'darkuhawkjhdjka@gmail.com', '$2a$10$4SdFS41MyimGAShPhSAOkePpaEWyJAeP7WLhVXjWjOyD21vtZxh8O', 'receptionist', 2, 4
);

INSERT INTO application_user (
id, active, email, password, username, application_user_role_id, location_id)
VALUES (
3, true, 'darkwwwwuahu@gmail.com', '$2a$10$XT/Cr8t4s/4zHYlkRXoDjuTa2JpepRSK1k7vk2mE5.femF5oL/Haq', 'administrator', 3, 4
);

INSERT INTO reservation (
id, reservation_date_start, reservation_date_end, application_user_id, car_id, location_id, reservation_state)
VALUES (1, parsedatetime('17-09-2012 18:47:52.69', 'dd-MM-yyyy hh:mm:ss.SS'),
 parsedatetime('17-09-2013 18:47:52.69', 'dd-MM-yyyy hh:mm:ss.SS'), 1, 1, 1, 'BOOKED');