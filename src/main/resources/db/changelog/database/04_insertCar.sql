--liquibase formatted sql
--changeset msiadul:4
INSERT INTO car (kilometers, model, location_id, archived)
VALUES (0, 'model1', 1, false);

INSERT INTO car (kilometers, model, location_id, archived)
VALUES (0, 'model2', 1, false);

INSERT INTO car (kilometers, model, location_id, archived)
VALUES (0, 'model3', 2, false);