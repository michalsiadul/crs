--liquibase formatted sql
--changeset msiadul:2
alter table application_user
add constraint FKm3bs4v6u9asp5avwu3oo3pipi
foreign key (application_user_role_id) references application_user_role;

alter table application_user
add constraint FKpdobdr2uwn1oyn5h4aucx2hb9
foreign key (location_id) references location;

alter table car
add constraint FKsjeo31bkla3haqy6x9hf5ij0o
foreign key (location_id) references location;

alter table application_user
drop constraint if exists email


