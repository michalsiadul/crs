--liquibase formatted sql
--changeset msiadul:5

INSERT INTO application_user (
active, email, password, username, application_user_role_id, location_id)
VALUES (
true, 'idehjadhajkdawkdh@gmail.com', '$2a$10$qW.LwKfTSJkrq79cFPEz8OTYSa3sCKJUy2fxTIw3k7y71Wj7qN.bi', 'employee', 1, 1);

INSERT INTO application_user (
active, email, password, username, application_user_role_id, location_id)
VALUES (
true, 'darkuhawkjhdjka@gmail.com', '$2a$10$4SdFS41MyimGAShPhSAOkePpaEWyJAeP7WLhVXjWjOyD21vtZxh8O', 'receptionist', 2, 4
);

INSERT INTO application_user (
active, email, password, username, application_user_role_id, location_id)
VALUES (
true, 'darkwwwwuahu@gmail.com', '$2a$10$XT/Cr8t4s/4zHYlkRXoDjuTa2JpepRSK1k7vk2mE5.femF5oL/Haq', 'administrator', 3, 4
);