package com.billennium.CarReservationSystem.security.jwt3;

import com.billennium.CarReservationSystem.entity.ApplicationUser;
import com.billennium.CarReservationSystem.repository.ApplicationUserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.NoSuchElementException;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    private ApplicationUserRepository userRepository;

    public UserDetailsServiceImpl(ApplicationUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ApplicationUser user =
                userRepository.findByUsername(username).orElseThrow(NoSuchElementException::new);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                Collections.singleton(
                        new SimpleGrantedAuthority(user.getApplicationUserRole().getName())));
    }
}