package com.billennium.CarReservationSystem.controller;

import com.billennium.CarReservationSystem.entity.Car;
import com.billennium.CarReservationSystem.entity.Reservation;
import com.billennium.CarReservationSystem.entity.dto.ReservationDTO;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchCarException;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchReservationException;
import com.billennium.CarReservationSystem.service.CarService;
import com.billennium.CarReservationSystem.service.ReservationService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.List;

@RestController
@PreAuthorize("hasAuthority('Administrator') or hasAuthority ('Receptionist')")
@RequestMapping("/receptionist")
public class ReceptionistController {

    private final CarService carService;
    private final ReservationService reservationService;

    public ReceptionistController(CarService carService, ReservationService reservationService) {
        this.carService = carService;
        this.reservationService = reservationService;
    }

    @GetMapping
    public List<ReservationDTO> findAllReservations() {
        return reservationService.findAll();
    }

    @GetMapping("/cars/{id}")
    public List<Car> findAllInGivenLocation(@PathVariable Long id) {
        return carService.findAllInGivenLocation(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ReservationDTO> getReservation(@PathVariable Long id) {
        return ResponseEntity.ok(reservationService.findById(id).orElseThrow(NoSuchReservationException::new));
    }

    @GetMapping("/car/{id}")
    public ResponseEntity<Car> getCar(@PathVariable Long id) {
        return ResponseEntity.ok(carService.findById(id).orElseThrow(NoSuchCarException::new));
    }

    @GetMapping("/cars")
    public List<Car> findAllCars() {
        return carService.findAll();
    }

    @PostMapping
    public ResponseEntity<Car> addCar(@RequestBody Car car) {
        return ResponseEntity.ok(carService.save(car));
    }

    @PostMapping("/reservation")
    public ResponseEntity<ReservationDTO> addReservationForAnyUser(@Valid @RequestBody Reservation reservation) throws Exception {
        return ResponseEntity.ok(reservationService.saveForAnyUser(reservation));
    }

    @DeleteMapping("/{id}")
    public void deleteCarById(@PathVariable Long id) {
        carService.deleteById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Car> updateCar(@Valid @RequestBody Car car, @PathVariable Long id) {
        return ResponseEntity.ok(carService.updateCar(car, id));
    }

    //dokonywanie wypożyczeń
    @PatchMapping("/state/{id}")
    public ResponseEntity<ReservationDTO> changeReservationStateToInProgress(@Valid @PathVariable Long id) throws MessagingException {
        return ResponseEntity.ok(reservationService.changeStateToInProgress(id));
    }

    //dokonywanie odbioru + delete w Employee Controller
    @PatchMapping("/{id}")
    public ResponseEntity<Car> updateCarKilometers(@RequestParam("km") int km, @PathVariable Long id) {
        return ResponseEntity.ok(carService.updateCarKilometers(km, id));
    }
}