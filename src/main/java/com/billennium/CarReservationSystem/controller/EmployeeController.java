package com.billennium.CarReservationSystem.controller;

import com.billennium.CarReservationSystem.entity.Reservation;
import com.billennium.CarReservationSystem.entity.dto.ReservationDTO;
import com.billennium.CarReservationSystem.service.ReservationService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;

@RestController
@PreAuthorize("hasAuthority('Administrator') or hasAuthority('Employee') or hasAuthority('Receptionist')")
@RequestMapping("/employee")
public class EmployeeController {

    private final ReservationService reservationService;

    public EmployeeController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PostMapping
    public ResponseEntity<ReservationDTO> addReservation(@Valid @RequestBody Reservation reservation) throws Exception {
        return ResponseEntity.ok(reservationService.save(reservation));
    }

    //todo: nie wyskakuje car doesnt exists, ps popraw id bledow
    @PutMapping("/{id}")
    public ResponseEntity<ReservationDTO> updateReservation(@Valid @RequestBody Reservation reservation, @PathVariable Long id) throws Exception {
        return ResponseEntity.ok(reservationService.updateReservation(reservation, id));
    }

    @PatchMapping("/datestart/{id}")
    public ResponseEntity<Reservation> updateReservationDateStart(@RequestBody Reservation reservation, @PathVariable Long id) throws MessagingException {
        return ResponseEntity.ok(reservationService.updateReservationDateStart(reservation, id));
    }

    @PatchMapping("/dateend/{id}")
    public ResponseEntity<Reservation> updateReservationDateEnd(@Valid @RequestBody Reservation reservation, @PathVariable Long id) throws MessagingException {
        return ResponseEntity.ok(reservationService.updateReservationDateEnd(reservation, id));
    }

    @PatchMapping("/car/{id}")
    public ResponseEntity<ReservationDTO> changeCarInReservation(@Valid @RequestBody Reservation reservation, @PathVariable Long id) throws Exception {
        return ResponseEntity.ok(reservationService.changeCarInReservation(reservation, id));
    }

    //zwrot auta
    @DeleteMapping("/{id}")
    public void deleteReservationById(@PathVariable Long id) {
        reservationService.deleteById(id);
    }

}