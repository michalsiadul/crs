package com.billennium.CarReservationSystem.controller;

import com.billennium.CarReservationSystem.entity.ApplicationUser;
import com.billennium.CarReservationSystem.entity.Location;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchLocationException;
import com.billennium.CarReservationSystem.service.ApplicationUserService;
import com.billennium.CarReservationSystem.service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@PreAuthorize("hasAuthority('Administrator')")
@RequestMapping("/admin")
public class AdministratorController {

    private final LocationService locationService;
    private final ApplicationUserService applicationUserService;

    public AdministratorController(LocationService locationService, ApplicationUserService applicationUserService) {
        this.locationService = locationService;
        this.applicationUserService = applicationUserService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Location> getLocation(@PathVariable Long id) {
        return ResponseEntity.ok(locationService.findById(id).orElseThrow(NoSuchLocationException::new));
    }

    @GetMapping
    public List<Location> findAllLocations() {
        return locationService.findAll();
    }

    @PostMapping
    public ResponseEntity<Location> addLocation(@Valid @RequestBody Location location) {
        return ResponseEntity.ok(locationService.save(location));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Location> updateLocation(@Valid @RequestBody Location location, @PathVariable Long id) {
        return ResponseEntity.ok(locationService.updateLocation(location, id));
    }

    @PatchMapping("/{userId}")
    public ResponseEntity<ApplicationUser> changeUserRoleToReceptionist(@PathVariable Long userId) {
        return ResponseEntity.ok(applicationUserService.changeUserRoleToReceptionist(userId));
    }

    @DeleteMapping("/{id}")
    public void deleteLocationById(@PathVariable Long id) {
        locationService.deleteById(id);
    }

    @DeleteMapping("/user/{id}")
    public void deleteUserById(@PathVariable Long id) {
        applicationUserService.deleteById(id);
    }
}