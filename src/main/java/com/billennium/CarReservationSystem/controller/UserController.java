package com.billennium.CarReservationSystem.controller;

import com.billennium.CarReservationSystem.entity.ApplicationUser;
import com.billennium.CarReservationSystem.repository.ApplicationUserRepository;
import com.billennium.CarReservationSystem.service.ApplicationUserRoleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.NoSuchElementException;

@RestController
@RequestMapping(value = "/user")
public class UserController {
    private final ApplicationUserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ApplicationUserRoleService applicationUserRoleService;

    public UserController(ApplicationUserRepository userRepository, PasswordEncoder passwordEncoder, ApplicationUserRoleService applicationUserRoleService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.applicationUserRoleService = applicationUserRoleService;
    }

    @PostMapping("/signup")
    @ApiOperation(value = "New user registration ")
    public void signUp(@RequestBody ApplicationUser user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setApplicationUserRole(
                applicationUserRoleService.findById(1L).orElseThrow(NoSuchElementException::new)
        );
        userRepository.save(user);
    }
}