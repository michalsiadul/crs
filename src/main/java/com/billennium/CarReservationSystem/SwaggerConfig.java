package com.billennium.CarReservationSystem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.InMemorySwaggerResourcesProvider;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@Configuration
public class SwaggerConfig {
    @Bean
    Docket api() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(Predicate.not(PathSelectors.regex("/error.*")))
                //.paths(PathSelectors.any())
                .build()
                .securitySchemes(securitySchemes());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Car Reservation System API")
                .description("System for renting vehicles within the company")
                .contact(new Contact("Michal Siadul",
                        "http://google.pl",
                        "example@example.email"))
                .license("No license at present")
                // .licenseUrl("URL to license")
                .version("1.0.0")
                .build();
    }

    @Primary
    @Bean
    public SwaggerResourcesProvider swaggerResourcesProvider(InMemorySwaggerResourcesProvider defaultResourcesProvider) {
        return () -> {
            SwaggerResource wsResource = new SwaggerResource();
            wsResource.setName("spec from yaml");
            wsResource.setSwaggerVersion("3.0");
            wsResource.setLocation("/openapi.yaml");
            List<SwaggerResource> resources = new ArrayList<>(defaultResourcesProvider.get());
            resources.clear(); // for only spec from yaml
            resources.add(wsResource);
            return resources;
        };
    }

    private static List<SecurityScheme> securitySchemes() {
        return List.of(new ApiKey("Authorization", "Bearer", "header"));
    }
}