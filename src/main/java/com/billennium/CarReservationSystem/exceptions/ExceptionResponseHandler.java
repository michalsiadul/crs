package com.billennium.CarReservationSystem.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLException;


@ControllerAdvice
@RestController
public class ExceptionResponseHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CustomException.class)
    public final ResponseEntity<CustomExceptionResponse> handleCustomExceptions(CustomException ex) {
        return ResponseEntity.status(ex.getHttpStatus()).body(new CustomExceptionResponse(ex));
    }

    @ExceptionHandler(RuntimeException.class)
    public final ResponseEntity<CustomExceptionResponse> handleAllExceptions(RuntimeException ex) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomExceptionResponse(ex));
    }

    @ExceptionHandler(SQLException.class)
    public final ResponseEntity<UsernameTakenException> handleAllExceptions() {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new UsernameTakenException());
    }
}