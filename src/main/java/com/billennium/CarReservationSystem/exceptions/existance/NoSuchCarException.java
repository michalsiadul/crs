package com.billennium.CarReservationSystem.exceptions.existance;

import com.billennium.CarReservationSystem.exceptions.CustomException;
import org.springframework.http.HttpStatus;

public class NoSuchCarException extends CustomException {
    public static final String ERROR_CODE = "006";
    public static final String ERROR_MESSAGE = "Car with this id doesn't exist";

    public NoSuchCarException() {
        super(ERROR_MESSAGE);
        this.errorCode = ERROR_CODE;
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}