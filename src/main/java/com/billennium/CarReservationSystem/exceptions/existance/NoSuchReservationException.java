package com.billennium.CarReservationSystem.exceptions.existance;

import com.billennium.CarReservationSystem.exceptions.CustomException;
import org.springframework.http.HttpStatus;


public class NoSuchReservationException extends CustomException {
    public static final String ERROR_CODE = "004";
    public static final String ERROR_MESSAGE = "Reservation with this id doesn't exist";

    public NoSuchReservationException() {
        super(ERROR_MESSAGE);
        this.errorCode = ERROR_CODE;
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}