package com.billennium.CarReservationSystem.exceptions.existance;

import com.billennium.CarReservationSystem.exceptions.CustomException;
import org.springframework.http.HttpStatus;

public class NoSuchLocationException extends CustomException {
    public static final String ERROR_CODE = "005";
    public static final String ERROR_MESSAGE = "Location with this id doesn't exist";

    public NoSuchLocationException() {
        super(ERROR_MESSAGE);
        this.errorCode = ERROR_CODE;
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}