package com.billennium.CarReservationSystem.exceptions.existance;

import com.billennium.CarReservationSystem.exceptions.CustomException;
import org.springframework.http.HttpStatus;

public class NoSuchUserException extends CustomException {
    public static final String ERROR_CODE = "007";
    public static final String ERROR_MESSAGE = "User with this id doesn't exist";

    public NoSuchUserException() {
        super(ERROR_MESSAGE);
        this.errorCode = ERROR_CODE;
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}