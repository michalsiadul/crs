package com.billennium.CarReservationSystem.exceptions;


import java.sql.SQLException;

public class UsernameTakenException extends SQLException {
    public static final String ERROR_MESSAGE = "Username taken";

    public UsernameTakenException() {
        super(ERROR_MESSAGE);
    }
}