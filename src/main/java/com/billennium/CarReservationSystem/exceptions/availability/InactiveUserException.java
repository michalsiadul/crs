package com.billennium.CarReservationSystem.exceptions.availability;

import com.billennium.CarReservationSystem.entity.ApplicationUser;
import com.billennium.CarReservationSystem.exceptions.CustomException;
import org.springframework.http.HttpStatus;

import java.util.Map;

public class InactiveUserException extends CustomException {

    public static final String ERROR_CODE = "001";
    public static final String ERROR_MESSAGE = "This user is not available at the moment";

    public InactiveUserException(ApplicationUser user) {
        super(String.format(ERROR_MESSAGE, user.toString()));
        this.errorCode = ERROR_CODE;
        this.objects = Map.of("user", user);
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}