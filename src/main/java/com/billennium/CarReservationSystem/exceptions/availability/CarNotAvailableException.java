package com.billennium.CarReservationSystem.exceptions.availability;

import com.billennium.CarReservationSystem.entity.Car;
import com.billennium.CarReservationSystem.exceptions.CustomException;
import org.springframework.http.HttpStatus;

import java.util.Map;

public class CarNotAvailableException extends CustomException {

    public static final String ERROR_CODE = "001";
    public static final String ERROR_MESSAGE = "This car is not available at the moment";

    public CarNotAvailableException(Car car) {
        super(String.format(ERROR_MESSAGE, car.toString()));
        this.errorCode = ERROR_CODE;
        this.objects = Map.of("car", car);
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }

    public CarNotAvailableException() {
        super("This car is not in User's location");
        this.errorCode = ERROR_CODE;
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}