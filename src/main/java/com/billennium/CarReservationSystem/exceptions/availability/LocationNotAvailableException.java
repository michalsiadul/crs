package com.billennium.CarReservationSystem.exceptions.availability;

import com.billennium.CarReservationSystem.entity.Location;
import com.billennium.CarReservationSystem.exceptions.CustomException;
import org.springframework.http.HttpStatus;

import java.util.Map;

public class LocationNotAvailableException extends CustomException {

    public static final String ERROR_CODE = "001";
    public static final String ERROR_MESSAGE = "This location is no longer available";

    public LocationNotAvailableException(Location location) {
        super(String.format(ERROR_MESSAGE, location.toString()));
        this.errorCode = ERROR_CODE;
        this.objects = Map.of("location", location);
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}