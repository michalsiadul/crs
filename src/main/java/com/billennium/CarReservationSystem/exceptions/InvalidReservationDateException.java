package com.billennium.CarReservationSystem.exceptions;

import com.billennium.CarReservationSystem.entity.Car;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.util.Map;

public class InvalidReservationDateException extends CustomException {

    public static final String ERROR_CODE = "008";
    public static final String ERROR_MESSAGE = "Reservation date start or end not acceptable";

    public InvalidReservationDateException(LocalDate localDate) {
        super(String.format(ERROR_MESSAGE, localDate.toString()));
        this.errorCode = ERROR_CODE;
        this.objects = Map.of("date", localDate);
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }


}