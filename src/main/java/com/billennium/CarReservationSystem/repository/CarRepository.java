package com.billennium.CarReservationSystem.repository;

import com.billennium.CarReservationSystem.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Long> {
}
