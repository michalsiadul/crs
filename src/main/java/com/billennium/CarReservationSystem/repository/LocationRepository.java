package com.billennium.CarReservationSystem.repository;

import com.billennium.CarReservationSystem.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location, Long> {
}