package com.billennium.CarReservationSystem.repository;

import com.billennium.CarReservationSystem.entity.ApplicationUserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ApplicationUserRoleRepository extends JpaRepository<ApplicationUserRole, Long> {
    Optional<ApplicationUserRole> findByName(String name);
}