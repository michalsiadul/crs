package com.billennium.CarReservationSystem.repository;

import com.billennium.CarReservationSystem.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
}