package com.billennium.CarReservationSystem.service;

import com.billennium.CarReservationSystem.entity.ApplicationUser;
import com.billennium.CarReservationSystem.entity.Reservation;
import com.billennium.CarReservationSystem.entity.dto.UserDTO;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchUserException;
import com.billennium.CarReservationSystem.repository.ApplicationUserRepository;
import com.billennium.CarReservationSystem.repository.ApplicationUserRoleRepository;
import com.billennium.CarReservationSystem.repository.ReservationRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class ApplicationUserService {
    private final ApplicationUserRepository applicationUserRepository;
    private final ReservationRepository reservationRepository;
    private final ApplicationUserRoleRepository applicationUserRoleRepository;

    public ApplicationUserService(ApplicationUserRepository applicationUserRepository, ReservationRepository reservationRepository, ApplicationUserRoleRepository applicationUserRoleRepository) {
        this.applicationUserRepository = applicationUserRepository;
        this.reservationRepository = reservationRepository;
        this.applicationUserRoleRepository = applicationUserRoleRepository;
    }

    public List<UserDTO> findAllinGivenLocation(Long id) {
        List<ApplicationUser> appUsersInGivenLocation = applicationUserRepository.findAll().stream()
                .filter(user -> user.getLocation().getId() == id)
                .filter(user -> !user.isActive())
                .toList();
        List<UserDTO> userDTOList = new ArrayList<>();
        for (ApplicationUser applicationUser : appUsersInGivenLocation) {
            userDTOList.add(new UserDTO(
                    applicationUser.getId(),
                    applicationUser.getUsername(),
                    applicationUser.isActive(),
                    applicationUser.getEmail(),
                    applicationUser.getApplicationUserRole(),
                    applicationUser.getLocation()
            ));
        }
        return userDTOList;
    }

    public void deleteById(Long id) {
        ApplicationUser applicationUser =
                applicationUserRepository.findById(id).orElseThrow(NoSuchUserException::new);
        List<Reservation> userReservations = findAllReservations(id);
        if (!userReservations.isEmpty()) {
            return;
        } else {
            applicationUser.setActive(false);
        }
        applicationUserRepository.save(applicationUser);
    }

    public List<Reservation> findAllReservations(Long id) {
        return reservationRepository.findAll().stream()
                .filter(reservation -> reservation.getApplicationUser().getId() == id)
                .toList();
    }

    public ApplicationUser changeUserRoleToReceptionist(Long userId) {
        ApplicationUser appuser = applicationUserRepository.findById(userId).orElseThrow(NoSuchUserException::new);
        appuser.setApplicationUserRole(
                applicationUserRoleRepository
                        .findById(2L).orElseThrow(NoSuchElementException::new)
        );
        return applicationUserRepository.save(appuser);
    }
}