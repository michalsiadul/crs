package com.billennium.CarReservationSystem.service;

import com.billennium.CarReservationSystem.entity.ApplicationUserRole;
import com.billennium.CarReservationSystem.entity.Car;
import com.billennium.CarReservationSystem.entity.Location;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchCarException;
import com.billennium.CarReservationSystem.repository.ApplicationUserRoleRepository;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ApplicationUserRoleService {

    private final ApplicationUserRoleRepository applicationUserRoleRepository;

    public ApplicationUserRoleService(ApplicationUserRoleRepository applicationUserRoleRepository) {
        this.applicationUserRoleRepository = applicationUserRoleRepository;
    }

    public ApplicationUserRole save(ApplicationUserRole applicationUserRole) {
        return applicationUserRoleRepository.save(applicationUserRole);
    }

    public Optional<ApplicationUserRole> findById(Long id) {
        return applicationUserRoleRepository.findById(id);
    }

    //For concurrency test
    @Transactional//(propagation = Propagation.REQUIRES_NEW)
    public void updateRoleNameWithoutWait(Long id, String roleName) {
        // Thread.sleep(5000);
            ApplicationUserRole beforeUpdate = findById(id)
                    .orElseThrow(NoSuchElementException::new);
            beforeUpdate.setName(roleName);
    }

    @Transactional//(propagation = Propagation.REQUIRES_NEW)
    public void updateRoleNameWithWait(Long id, String roleName) throws InterruptedException {
        Thread.sleep(5000);
        ApplicationUserRole beforeUpdate = findById(id)
                .orElseThrow(NoSuchElementException::new);
        beforeUpdate.setName(roleName);
    }
}
