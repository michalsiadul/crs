package com.billennium.CarReservationSystem.service;

import com.billennium.CarReservationSystem.email.EmailService;
import com.billennium.CarReservationSystem.entity.ApplicationUser;
import com.billennium.CarReservationSystem.entity.Car;
import com.billennium.CarReservationSystem.entity.Location;
import com.billennium.CarReservationSystem.entity.Reservation;
import com.billennium.CarReservationSystem.entity.dto.ReservationDTO;
import com.billennium.CarReservationSystem.entity.dto.UserDTO;
import com.billennium.CarReservationSystem.entity.state.ReservationState;
import com.billennium.CarReservationSystem.exceptions.availability.CarNotAvailableException;
import com.billennium.CarReservationSystem.exceptions.availability.InactiveUserException;
import com.billennium.CarReservationSystem.exceptions.availability.LocationNotAvailableException;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchCarException;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchLocationException;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchReservationException;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchUserException;
import com.billennium.CarReservationSystem.repository.ApplicationUserRepository;
import com.billennium.CarReservationSystem.repository.ReservationRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.*;

@Service
public class ReservationService {

    private final ReservationRepository reservationRepository;
    private final ApplicationUserRepository applicationUserRepository;
    private final LocationService locationService;
    private final CarService carService;
    private final EmailService mailService;

    public ReservationService(ReservationRepository reservationRepository, LocationService locationService, CarService carService, ApplicationUserRepository applicationUserRepository,
                              EmailService mailService) {
        this.reservationRepository = reservationRepository;
        this.locationService = locationService;
        this.carService = carService;
        this.applicationUserRepository = applicationUserRepository;
        this.mailService = mailService;
    }

    public List<ReservationDTO> findAll() {
        List<Reservation> allReservations = reservationRepository.findAll();
        List<ReservationDTO> reservationDTOList = new ArrayList<>();
        for (Reservation allReservation : allReservations) {
            UserDTO userDTO = createUserDto(allReservation.getApplicationUser());
            reservationDTOList.add(createReservationDTO(allReservation, userDTO));
        }
        return reservationDTOList;
    }

    public ReservationDTO save(Reservation reservation) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        //todo: można spróbować tak, że jak ktoś ma uprawnienia (z contextu) i ciało request
        // jest z applicationuser, to dodajemy tego z body
        ApplicationUser applicationUser =
                applicationUserRepository.findByUsername(username).orElseThrow(NoSuchUserException::new);
        UserDTO userDTO = createUserDto(applicationUser);
        validateIfUserIsActive(applicationUser, reservation);
        Location location =
                locationService.findById(
                        applicationUser.getLocation().getId()
                ).orElseThrow(NoSuchLocationException::new);
        validateIfLocationIsActive(location, reservation);
        Car car =
                carService.findById(reservation.getCar().getId()).orElseThrow(NoSuchCarException::new);
        List<Car> unavailableCarList = getUnavailableCarsList(car, reservation);
        if (!car.isArchived() && !unavailableCarList.contains(car)) {
            reservation.setCar(car);
            carService.save(car);
            reservation.setReservationState(ReservationState.BOOKED);
            if (applicationUser.getLocation().getId() == reservation.getCar().getLocation().getId()) {
               reservationRepository.save(reservation);
            } else {
                throw new CarNotAvailableException();
            }
        } else {
            throw new CarNotAvailableException(car);
        }
        ReservationDTO reservationDTO = createReservationDTO(reservation, userDTO);
        sendEmail(reservationDTO, applicationUser, "created");
        return reservationDTO;
    }

    public Optional<ReservationDTO> findById(Long id) {
        Reservation reservation = reservationRepository.findById(id).orElseThrow(NoSuchReservationException::new);
        UserDTO userDTO = new UserDTO(
                reservation.getApplicationUser().getId(),
                reservation.getApplicationUser().getUsername(),
                reservation.getApplicationUser().isActive(),
                reservation.getApplicationUser().getEmail(),
                reservation.getApplicationUser().getApplicationUserRole(),
                reservation.getApplicationUser().getLocation()
        );
        ReservationDTO reservationDTO = createReservationDTO(reservation, userDTO);
        return Optional.of(reservationDTO);
    }

    public void deleteById(Long id) {
        Reservation reservation = reservationRepository.findById(id).orElseThrow(NoSuchReservationException::new);
        reservation.setReservationState(ReservationState.ARCHIVED);
        Car reservedCar = reservation.getCar();
        reservationRepository.save(reservation);
        carService.save(reservedCar);
    }

    public ReservationDTO updateReservation(Reservation reservation, Long id) throws Exception {
        Reservation beforeUpdate = reservationRepository.findById(id)
                .orElseThrow(NoSuchReservationException::new);
        beforeUpdate.setReservationDateStart(reservation.getReservationDateStart());
        beforeUpdate.setReservationDateEnd(reservation.getReservationDateEnd());
        beforeUpdate.setCar(reservation.getCar());
        beforeUpdate.setLocation(reservation.getLocation());
        beforeUpdate.setApplicationUser(reservation.getApplicationUser());
        beforeUpdate.setReservationState(reservation.getReservationState());
        if (beforeUpdate != null) {
            mailService.sendMail(getUserEmail(beforeUpdate.getApplicationUser()),
                    "Reservation Update",
                    "Your reservation was updated successfully",
                    false);
        }
        sendEmail(beforeUpdate, beforeUpdate.getApplicationUser(), "updated");
        return save(beforeUpdate);
    }

    public Reservation updateReservationDateStart(Reservation reservation, Long id) throws MessagingException {
        Reservation beforeUpdate = reservationRepository.findById(id)
                .orElseThrow(NoSuchReservationException::new);
        beforeUpdate.setReservationDateStart(reservation.getReservationDateStart());
        sendEmail(beforeUpdate, beforeUpdate.getApplicationUser(), "updated");
        return reservationRepository.save(beforeUpdate);
    }

    public Reservation updateReservationDateEnd(Reservation reservation, Long id) throws MessagingException {
        Reservation beforeUpdate = reservationRepository.findById(id)
                .orElseThrow(NoSuchReservationException::new);
        beforeUpdate.setReservationDateEnd(reservation.getReservationDateEnd());
        sendEmail(beforeUpdate, beforeUpdate.getApplicationUser(), "updated");
        return reservationRepository.save(beforeUpdate);
    }

    public ReservationDTO changeCarInReservation(Reservation reservation, Long id) throws Exception {
        Reservation beforeUpdate = reservationRepository.findById(id)
                .orElseThrow(NoSuchReservationException::new);
        beforeUpdate.setCar(reservation.getCar());
        sendEmail(beforeUpdate, beforeUpdate.getApplicationUser(), "updated");
        return save(beforeUpdate);
    }

    public ReservationDTO changeStateToInProgress(Long id) throws MessagingException {
        Reservation beforeUpdate = reservationRepository.findById(id)
                .orElseThrow(NoSuchReservationException::new);
        beforeUpdate.setReservationState(ReservationState.IN_PROGRESS);
        reservationRepository.save(beforeUpdate);
        ReservationDTO reservationDTO = findById(id)
                .orElseThrow(NoSuchReservationException::new);
        reservationDTO.setReservationState(ReservationState.IN_PROGRESS);
        sendEmail(beforeUpdate, beforeUpdate.getApplicationUser(), "updated");
        return reservationDTO;
    }

    public List<Reservation> findReservationsByCar(Long carId) {
        return reservationRepository.findAll().stream()
                .filter(reservation -> reservation.getCar().getId() == carId)
                .toList();
    }

    public ReservationDTO saveForAnyUser(Reservation reservation) throws Exception {
        ApplicationUser applicationUser =
                applicationUserRepository.findById(reservation.getApplicationUser().getId())
                        .orElseThrow(NoSuchUserException::new);
        UserDTO userDTO = createUserDto(applicationUser);
        validateIfUserIsActive(applicationUser, reservation);
        Location location =
                locationService.findById(
                        applicationUser.getLocation().getId()
                ).orElseThrow(NoSuchLocationException::new);
        validateIfLocationIsActive(location, reservation);
        Car car =
                carService.findById(reservation.getCar().getId()).orElseThrow(NoSuchCarException::new);
        List<Car> unavailableCarList = getUnavailableCarsList(car, reservation);
        if (!car.isArchived() && !unavailableCarList.contains(car)) {
            reservation.setCar(car);
            carService.save(car);
            reservation.setReservationState(ReservationState.BOOKED);
            if (applicationUser.getLocation().getId() == reservation.getCar().getLocation().getId()) {
                reservationRepository.save(reservation);
            } else {
                throw new CarNotAvailableException();
            }
        } else {
            throw new CarNotAvailableException(car);
        }
        ReservationDTO reservationDTO = createReservationDTO(reservation, userDTO);
        sendEmail(reservationDTO, applicationUser, "created");
        return reservationDTO;
    }

    private ReservationDTO createReservationDTO(Reservation reservation, UserDTO userDTO) {
        return new ReservationDTO(
                reservation.getId(),
                reservation.getLocation(),
                reservation.getCar(),
                userDTO,
                reservation.getReservationDateStart(),
                reservation.getReservationDateEnd(),
                reservation.getReservationState()
        );
    }

    private UserDTO createUserDto(ApplicationUser applicationUser) {
        return new UserDTO(
                applicationUser.getId(),
                applicationUser.getUsername(),
                applicationUser.isActive(),
                applicationUser.getEmail(),
                applicationUser.getApplicationUserRole(),
                applicationUser.getLocation()
        );
    }

    private void validateIfUserIsActive(ApplicationUser applicationUser, Reservation reservation) {
        if (applicationUser.isActive()) {
            reservation.setApplicationUser(applicationUser);
        } else {
            throw new InactiveUserException(applicationUser);
        }
    }

    private void validateIfLocationIsActive(Location location, Reservation reservation) {
        if (!location.isArchived()) {
            reservation.setLocation(location);
        } else {
            throw new LocationNotAvailableException(location);
        }
    }


    private List<Car> getUnavailableCarsList(Car car, Reservation reservation) {
        List<Car> unavailableCarList = new ArrayList<>();
        List<Reservation> reservationsByCar = findReservationsByCar(car.getId()).stream()
                .filter(r ->
                        !r.getReservationDateEnd().isBefore(reservation.getReservationDateStart()))
                .filter(r ->
                        !r.getReservationDateStart().isAfter(reservation.getReservationDateEnd()))
                .toList();
        for (Reservation value : reservationsByCar) {
            unavailableCarList.add(
                    value.getCar()
            );
        }
        return unavailableCarList;
    }

    private String getUserEmail(ApplicationUser applicationUser) {
        return applicationUser.getEmail();
    }

    private void sendEmail(Object obj, ApplicationUser applicationUser, String keyWord) throws MessagingException {
        if (obj != null) {
            mailService.sendMail(getUserEmail(applicationUser),
                    "Reservation " + keyWord,
                    "Your reservation was " + keyWord + " successfully",
                    false);
        }
    }
}