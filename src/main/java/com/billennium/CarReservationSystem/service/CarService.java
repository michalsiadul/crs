package com.billennium.CarReservationSystem.service;

import com.billennium.CarReservationSystem.entity.Car;
import com.billennium.CarReservationSystem.entity.Location;
import com.billennium.CarReservationSystem.entity.Reservation;
import com.billennium.CarReservationSystem.exceptions.availability.LocationNotAvailableException;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchCarException;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchLocationException;
import com.billennium.CarReservationSystem.repository.CarRepository;
import com.billennium.CarReservationSystem.repository.LocationRepository;
import com.billennium.CarReservationSystem.repository.ReservationRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;


@Service

public class CarService {

    private final CarRepository carRepository;
    private final LocationRepository locationRepository;
    private final ReservationRepository reservationRepository;

    public CarService(CarRepository carRepository, LocationRepository locationRepository, ReservationRepository reservationRepository) {
        this.carRepository = carRepository;
        this.locationRepository = locationRepository;
        this.reservationRepository = reservationRepository;
    }

    public Car save(Car car) throws LocationNotAvailableException {
        Location location =
                locationRepository.findById(car.getLocation().getId()).orElseThrow(NoSuchLocationException::new);
        if (!location.isArchived()) {
            car.setLocation(location);
            return carRepository.save(car);
        } else
            throw new LocationNotAvailableException(location);
    }


    public Optional<Car> findById(Long id) {
        return carRepository.findById(id);
    }

    public Car updateCar(Car car, Long id) {
        Car beforeUpdate = findById(id)
                .orElseThrow(NoSuchCarException::new);
        beforeUpdate.setKilometers(car.getKilometers());
        beforeUpdate.setLocation(car.getLocation());
        beforeUpdate.setModel(car.getModel());
        beforeUpdate.setArchived(car.isArchived());
        return save(beforeUpdate);
    }

    public Car updateCarKilometers(Integer km, Long id) {
        Car beforeUpdate = findById(id)
                .orElseThrow(NoSuchCarException::new);
        beforeUpdate.setKilometers(km);
        return save(beforeUpdate);
    }

    @Transactional
    public void deleteById(Long id) {
        Car toArchive = findById(id).orElseThrow(NoSuchCarException::new);
        List<Reservation> reservations = reservationRepository.findAll();
        for (Reservation reservation : reservations) {
            if (reservation.getCar().getId() == toArchive.getId())
                return;
        }
        toArchive.setArchived(true);
      //  carRepository.save(toArchive);
    }

    public List<Car> findAll() {
        return carRepository.findAll();
    }

    public List<Car> findAllInGivenLocation(Long id) {
        return carRepository.findAll().stream()
                .filter(car -> car.getLocation().getId() == id)
                .filter(car -> !car.isArchived())
                .toList();
    }
}