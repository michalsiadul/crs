package com.billennium.CarReservationSystem.service;

import com.billennium.CarReservationSystem.entity.Car;
import com.billennium.CarReservationSystem.entity.Location;
import com.billennium.CarReservationSystem.entity.dto.UserDTO;
import com.billennium.CarReservationSystem.exceptions.existance.NoSuchLocationException;
import com.billennium.CarReservationSystem.repository.LocationRepository;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class LocationService {
    private final LocationRepository locationRepository;
    private final CarService carService;
    private final ApplicationUserService applicationUserService;

    public LocationService(LocationRepository locationRepository, CarService carService, ApplicationUserService applicationUserService) {
        this.locationRepository = locationRepository;
        this.carService = carService;
        this.applicationUserService = applicationUserService;
    }

    public Location save(Location location) {
        return locationRepository.save(location);
    }

    public Optional<Location> findById(Long aLong) {
        return locationRepository.findById(aLong);
    }

    public void deleteById(Long id) {
        Location toArchived =
                locationRepository.findById(id).orElseThrow(NoSuchLocationException::new);
        List<Car> carsInGiveLocation = carService.findAllInGivenLocation(id);
        List<UserDTO> usersInGivenLocation = applicationUserService.findAllinGivenLocation(id);
        if (
                (!carsInGiveLocation.isEmpty())
                        || (!usersInGivenLocation.isEmpty())
        ) {
            return;
        } else {
            toArchived.setArchived(true);
            locationRepository.save(toArchived);
        }
    }

    public Location updateLocation(Location location, Long id) {
        Location beforeUpdate = findById(id)
                .orElseThrow(NoSuchLocationException::new);
        beforeUpdate.setName(location.getName());
        beforeUpdate.setArchived(location.isArchived());
        return save(beforeUpdate);
    }

    public List<Location> findAll() {
        return locationRepository.findAll();
    }
}