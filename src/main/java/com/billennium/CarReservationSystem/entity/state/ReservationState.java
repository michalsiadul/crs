package com.billennium.CarReservationSystem.entity.state;

public enum ReservationState {
    BOOKED,
    IN_PROGRESS,
    ARCHIVED
}