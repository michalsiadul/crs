package com.billennium.CarReservationSystem.entity.dto;

import com.billennium.CarReservationSystem.entity.ApplicationUserRole;
import com.billennium.CarReservationSystem.entity.Location;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private long id;
    private String username;
    private boolean active = true;
    private String email;
    private ApplicationUserRole applicationUserRole;
    private Location location;
}
