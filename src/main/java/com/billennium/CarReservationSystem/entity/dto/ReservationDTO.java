package com.billennium.CarReservationSystem.entity.dto;

import com.billennium.CarReservationSystem.entity.Car;
import com.billennium.CarReservationSystem.entity.Location;
import com.billennium.CarReservationSystem.entity.state.ReservationState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReservationDTO {
    private long id;
    private Location location;
    private Car car;
    private UserDTO userDTO;
    private LocalDate reservationDateStart;
    private LocalDate reservationDateEnd;
    private ReservationState reservationState;
}