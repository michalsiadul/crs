package com.billennium.CarReservationSystem.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ApiModelProperty("An archived vehicle cannot be rented")
    private boolean archived = false;
    @NotEmpty(message = "model name must not be empty")
    @Size(min = 2, max = 32, message = "model name must be between 2 and 32 characters long")
    private String model;
    @ApiModelProperty("the vehicle's mileage")
    private int kilometers = 0;
    @ManyToOne
    @NotNull
    private Location location;
}