package com.billennium.CarReservationSystem.entity;

import com.billennium.CarReservationSystem.entity.state.ReservationState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @OneToOne
    private Location location;
    @ManyToOne
    @NotNull
    private Car car;
    @ManyToOne
    private ApplicationUser applicationUser;
    @NotNull
    private LocalDate reservationDateStart;
    @NotNull
    private LocalDate reservationDateEnd;
    @Enumerated(EnumType.STRING)
    private ReservationState reservationState;
}