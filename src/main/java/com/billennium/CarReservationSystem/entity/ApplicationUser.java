package com.billennium.CarReservationSystem.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(uniqueConstraints = {
        @UniqueConstraint(name = "email", columnNames = {"email"}),
        @UniqueConstraint(name = "username", columnNames = {"username"})
})
public class ApplicationUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty("The User's id") //property description in the schema field
    private long id;
    @NotEmpty(message = "Name must not be empty")
    @Size(min = 2, max = 32, message = "Name must be between 2 and 32 characters long")
    private String username;
    @NotEmpty(message = "Password must not be empty")
    @Size(min = 5, message = "Password must be at least 5 characters long")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    @ApiModelProperty("describes if the account is active - capable of using the system")
    private boolean active = true;
    @NotEmpty(message = "Email must not be empty")
    @Size(min = 2, max = 32, message = "email must be between 2 and 32 characters long")
    private String email;
    @ManyToOne
    @ApiModelProperty("system permissions")
    private ApplicationUserRole applicationUserRole;
    @OneToOne
    @NotNull
    private Location location;

    public ApplicationUser(String username, String email, String password, Location location) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.location = location;
    }

    @Override
    public String toString() {
        return "ApplicationUser{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", active=" + active +
                ", email='" + email + '\'' +
                ", applicationUserRole=" + applicationUserRole +
                ", location=" + location +
                '}';
    }
}